# Process to onboard partners/addresses/users

1. While doing first deployment, create an admin user using set_up_admin.py
2. Login using Admin user and Admin partner code
3. Then Create a new partner
4. Using the output partner code, create a new address for this partner. This will give a location id.
5. Use the location id and partner code, create users.