from typing import List
from devtools import debug
from datetime import date, datetime
import sqlalchemy as sa
from app.core.security import hash_password
from app.config import settings
from app.api.v1.partner.models.partner_model import Partner
from app.api.v1.partner.schemas.partner_schema import Tier
from app.api.v1.partner.models.partner_address_model import PartnerAddress
from app.api.v1.auth.models.user_model import User

DB_USER = settings.DB_USER
DB_PASS = settings.DB_PASS
DB_HOST = settings.DB_HOST
DB_PORT = settings.DB_PORT
DB_NAME = settings.DB_NAME
DB_URL = f"postgresql://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"


def get_session():
    engine = sa.create_engine(DB_URL)

    return sa.orm.Session(engine)


# TODO: Make this script take inputs from sysadmin while deploying for the first time


def create_admin(session):
    hp_partner = Partner(
        name="HealthPe",
        is_active=True,
        is_banned=False,
        ptype="Admin",
        onboarding_date=date.today(),
        tier=Tier.standalone,
    )

    session.add(hp_partner)
    session.flush()
    session.refresh(hp_partner)
    debug(f"Admin partner code {hp_partner.code} added")
    hp_admin_dummy_address = PartnerAddress(
        atype="Admin",
        line_1="HealthPe HQ",
        city="Bangalore",
        state="Karnataka",
        country="India",
        pincode="560077",
        gstin="Dummy",
        partner_code=hp_partner.code,
        is_active=True,
        is_banned=False,
        drug_license_number="Dummy",
        pan_card_no="Dummy",
    )
    session.add(hp_admin_dummy_address)
    session.flush()
    session.refresh(hp_admin_dummy_address)
    debug(f"Admin partner address {hp_admin_dummy_address.id} added")

    admin_user = User(
        first_name="Admin",
        last_name="Admin",
        username="admin",
        hashed_password=hash_password("admin"),
        phone_number="Admin",
        email="admin@healthpe.com",
        is_active=True,
        partner_code=hp_partner.code,
        location_id=hp_admin_dummy_address.id,
        otp="",
        last_login=None,
        last_password_change_time=datetime.now(),
        role="Admin",
        poc=False,
        id_proof_object_name="Dummy",
        manager_id=None,
        admin_id=None,
    )

    session.add(admin_user)
    session.flush()
    session.refresh(admin_user)
    debug(f"Admin user {admin_user.id} added")

    # session.commit()


def create_partner(session, partner_type: str, partner_name: str, associates: List[str], managers: List[str]):
    pharmacy_partner = Partner(
        name=partner_name,
        is_active=True,
        is_banned=False,
        ptype=partner_type,
        onboarding_date=date.today(),
        tier=Tier.standalone,
    )

    session.add(pharmacy_partner)
    session.flush()
    session.refresh(pharmacy_partner)
    debug(f"{partner_name} partner code {pharmacy_partner.code} added")
    pharmacy_address = PartnerAddress(
        atype="Shop",
        line_1=f"{partner_name} HQ",
        city="Bangalore",
        state="Karnataka",
        country="India",
        pincode="560077",
        gstin="Dummy",
        partner_code=pharmacy_partner.code,
        is_active=True,
        is_banned=False,
        drug_license_number="Dummy",
        pan_card_no="Dummy",
    )
    session.add(pharmacy_address)
    session.flush()
    session.refresh(pharmacy_address)
    debug(f"{partner_name} partner address {pharmacy_address.id} added")

    for username in associates:
        pharmacy_user_associate = User(
            first_name=f"{partner_name}_{username}_first",
            last_name=f"{partner_name}_{username}_last",
            username=username,
            hashed_password=hash_password("pass"),
            phone_number="1234567890",
            email=f"{partner_name.strip().replace(' ','')}@partners.com",
            is_active=True,
            partner_code=pharmacy_partner.code,
            location_id=pharmacy_address.id,
            otp="",
            last_login=None,
            last_password_change_time=datetime.now(),
            role="Associate",
            poc=False,
            id_proof_object_name="Dummy",
            manager_id=None,
            admin_id=None,
        )

        session.add(pharmacy_user_associate)
        session.flush()
        session.refresh(pharmacy_user_associate)
        debug(f"Associate User {username} added to {partner_name}")


def create_pharmacy(session, pharmacy_name: str, associates: List[str], managers: List[str]):
    pharmacy_partner = Partner(
        name=pharmacy_name,
        is_active=True,
        is_banned=False,
        ptype="Fulfillment",
        onboarding_date=date.today(),
        tier=Tier.standalone,
    )

    session.add(pharmacy_partner)
    session.flush()
    session.refresh(pharmacy_partner)
    debug(f"{pharmacy_name} partner code {pharmacy_partner.code} added")
    pharmacy_address = PartnerAddress(
        atype="Shop",
        line_1=f"{pharmacy_name} HQ",
        city="Bangalore",
        state="Karnataka",
        country="India",
        pincode="560077",
        gstin="Dummy",
        partner_code=pharmacy_partner.code,
        is_active=True,
        is_banned=False,
        drug_license_number="Dummy",
        pan_card_no="Dummy",
    )
    session.add(pharmacy_address)
    session.flush()
    session.refresh(pharmacy_address)
    debug(f"{pharmacy_name} partner address {pharmacy_address.id} added")

    for username in associates:
        pharmacy_user_associate = User(
            first_name="Associate",
            last_name="Associate",
            username=username,
            hashed_password=hash_password("pass"),
            phone_number="1234567890",
            email=f"{pharmacy_name}@pharmacy-1.com",
            is_active=True,
            partner_code=pharmacy_partner.code,
            location_id=pharmacy_address.id,
            otp="",
            last_login=None,
            last_password_change_time=datetime.now(),
            role="Associate",
            poc=False,
            id_proof_object_name="Dummy",
            manager_id=None,
            admin_id=None,
        )

        session.add(pharmacy_user_associate)
        session.flush()
        session.refresh(pharmacy_user_associate)
        debug(f"Associate User {username} added to {pharmacy_name}")


def clean_all_data(session):

    session.execute("delete from fulfillment_requests")
    session.execute("delete from order_items")
    session.execute("delete from order_history")
    session.execute("delete from orders")
    session.execute("delete from customer_addresses")
    session.execute("delete from customers")
    session.execute("delete from partner_source")
    session.execute("delete from users")
    session.execute("delete from partner_addresses")
    session.execute("delete from partners")


# if __name__ == "__main__":

#     with get_session() as session:

#         create_admin(session)
#         create_pharmacy(session, pharmacy_name="Pharmacy 1", associates=["ph_1_ass_1", "ph_1_ass_1"], managers=[])
#         session.commit()

#     with get_session() as session:
#         clean_all_data(session)
#         session.commit()
