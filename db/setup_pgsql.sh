docker pull postgis/postgis:14-master
docker run --name postgis -p 5432:5432 -e POSTGRES_PASSWORD=admin -d postgis/postgis:14-master

# Connect to postgres DB using postgres:admin

# Run the following on POSTGRES Admin
# create database healthpe;

# create user app_user with password 'app_pass' nocreatedb;

# Run the following as admin on healthpe
# grant insert, update, delete, select on all tables in schema public to app_user;
# create extension postgis;