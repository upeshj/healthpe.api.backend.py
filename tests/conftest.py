import os
import asyncio
import pytest
import httpx
import json
import random, string
from starlette.testclient import TestClient
from sqlalchemy.sql import select
from db.set_up_admin import create_admin, get_session, create_partner, clean_all_data
from app import main


BASE_URL = "http://localhost:8000/"
LOGIN_ENDPOINT = "v1/auth/login"

PHARMACY_1_ASSOCIATE_1 = "ph_1_ass_1"
PHARMACY_1_ASSOCIATE_2 = "ph_1_ass_2"
PHARMACY_2_ASSOCIATE_1 = "ph_2_ass_1"
PHARMACY_2_ASSOCIATE_2 = "ph_2_ass_2"

SALES_1_ASSOCIATE_1 = "sl_1_ass_1"
SALES_1_ASSOCIATE_2 = "sl_1_ass_2"

HP_1_ASSOCIATE_1 = "hp_1_ass_1"
HP_1_ASSOCIATE_2 = "hp_1_ass_2"


@pytest.fixture(scope="session")
def event_loop():
    """
    This fixture is important to have the entire session run properly in the same event loop.
    If this fixture is not used, you will get the following RunTimeError:
    RuntimeError: Task <Task pending name='starlette.middleware.base.BaseHTTPMiddleware.__call__.<locals>.call_next.<locals>.coro' coro=<BaseHTTPMiddleware.__call__.<locals>.call_next.<locals>.coro() running at /Users/upeshjindal/Virtual/healthpe.api.backend.py/lib/python3.10/site-packages/starlette/middleware/base.py:30> cb=[TaskGroup._spawn.<locals>.task_done() at /Users/upeshjindal/Virtual/healthpe.api.backend.py/lib/python3.10/site-packages/anyio/_backends/_asyncio.py:622]> got Future <Future pending cb=[Protocol._on_waiter_completed()]> attached to a different loop
    At session.execute()

    """
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session", autouse=True)
async def setup_db(event_loop):

    with get_session() as session:
        clean_all_data(session)
        session.execute("ALTER SEQUENCE users_id_seq RESTART WITH 1")
        session.execute("ALTER SEQUENCE partners_code_seq RESTART WITH 1")
        session.execute("ALTER SEQUENCE partner_addresses_id_seq RESTART WITH 1")
        create_admin(session)
        create_partner(
            session,
            partner_type="Fulfillment",
            partner_name="Pharmacy 1",
            associates=[PHARMACY_1_ASSOCIATE_1, PHARMACY_1_ASSOCIATE_2],
            managers=[],
        )
        create_partner(
            session,
            partner_type="Fulfillment",
            partner_name="Pharmacy 2",
            associates=[PHARMACY_2_ASSOCIATE_1, PHARMACY_2_ASSOCIATE_2],
            managers=[],
        )
        create_partner(
            session,
            partner_type="Sales",
            partner_name="Sales Team",
            associates=[SALES_1_ASSOCIATE_1, SALES_1_ASSOCIATE_2],
            managers=[],
        )
        create_partner(
            session,
            partner_type="Delivery",
            partner_name="HealthPe Team",
            associates=[HP_1_ASSOCIATE_1, HP_1_ASSOCIATE_2],
            managers=[],
        )

        session.commit()

    yield


@pytest.fixture(scope="session")
def new_partner():

    return {"name": "Apollo Pharmacy", "ptype": "Fulfillment", "tier": "Standalone"}


@pytest.fixture(scope="function")
async def test_app():
    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        yield client


@pytest.fixture(scope="function")
async def admin_user(event_loop, setup_db):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        # TODO: Need credentials from somewhere in the env
        response = await client.post(
            "v1/auth/login",
            json={"username": "admin", "password": "admin", "partner_code": 1},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as admin:

            yield admin


@pytest.fixture(scope="session")
async def ph_1_associate_1(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": PHARMACY_1_ASSOCIATE_1, "password": "pass", "partner_code": 2},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as ph_ass:

            yield ph_ass


@pytest.fixture(scope="session")
async def ph_1_associate_2(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": PHARMACY_1_ASSOCIATE_2, "password": "pass", "partner_code": 2},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as ph_ass:

            yield ph_ass


@pytest.fixture(scope="session")
async def sales_team_1_associate_1(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": SALES_1_ASSOCIATE_1, "password": "pass", "partner_code": 4},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as sl_ass:

            yield sl_ass


@pytest.fixture(scope="session")
async def sales_team_1_associate_2(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": SALES_1_ASSOCIATE_1, "password": "pass", "partner_code": 4},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as sl_ass:

            yield sl_ass


@pytest.fixture(scope="session")
async def hp_team_1_associate_1(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": HP_1_ASSOCIATE_1, "password": "pass", "partner_code": 5},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as sl_ass:

            yield sl_ass


@pytest.fixture(scope="session")
async def hp_team_1_associate_2(event_loop):

    async with httpx.AsyncClient(app=main.app, base_url=BASE_URL) as client:

        response = await client.post(
            "v1/auth/login",
            json={"username": HP_1_ASSOCIATE_2, "password": "pass", "partner_code": 5},
        )

        print(response.json())

        headers = {"Authorization": f"Bearer {response.json()['access_token']}"}
        async with httpx.AsyncClient(app=main.app, base_url=BASE_URL, headers=headers) as sl_ass:

            yield sl_ass


@pytest.fixture(scope="function")
def new_customer_new_order():

    # data = {"order": 1}
    order = {
        "customer": {"first_name": "Upesh", "phone_number": "+917420008561"},
        "items": [{"name": "Medicine 234", "quantity": 10}, {"name": "Medicine 500", "quantity": 15}],
        "delivery_address": {
            "atype": "Home",
            "line_1": "Somewhere in the middle",
            "city": "Kochi",
            "state": "Kerala",
            "country": "India",
            "pincode": "1234567",
        },
    }

    data = {"order": (None, json.dumps(order))}

    return data


@pytest.fixture(scope="function")
def new_pharmacy_user():

    phone = "".join(random.choices(string.ascii_lowercase, k=12))

    user = {
        "first_name": "First",
        "last_name": "Last",
        "username": phone,
        "password": "pass",
        "email": "ph_2_user_1@example.com",
        "phone_number": phone,
        "partner_code": 2,
        "location_id": 2,
        "role": "Associate",
        "poc": True,
        "manager_id": 1,
        "admin_id": 1,
    }

    return user


@pytest.fixture(scope="function")
def dummy_file():

    with open(os.devnull, "rb") as file:
        yield file
