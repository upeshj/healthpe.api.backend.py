import asyncio
from datetime import datetime
from unittest import mock
from unittest.mock import patch
import pytest
from app.api.v1.ordering.repositories.order_repository import OrderRepository
from app.api.v1.ordering.models.order_model import Order, OrderStatus
from app.api.v1.ordering.models.order_item_model import OrderItem
from sqlalchemy.sql import visitors, ColumnElement
from sqlalchemy.exc import NoResultFound
from app.core.errors import DBError


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_init_calls_base_init(mocked_super):

    repo = OrderRepository()

    mocked_super.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.select")
@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_get_order_by_id_returns_order_when_found(base_repo, select):

    mock_order = mock.Mock(spec=Order)
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.return_value = (
        mock.Mock()
    )  # This is not AsyncMock because the calls further are not coroutines
    repo.session.execute.return_value.unique.return_value.one.return_value = (mock_order,)

    order = await repo.get_order_by_id(1)

    select.assert_called_once_with(Order)
    repo.session.execute.assert_called_once()
    assert isinstance(order, Order)


@patch("app.api.v1.ordering.repositories.order_repository.select")
@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_get_order_by_id_returns_None_when_NoResultFound_Exception(base_repo, select):

    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.return_value = mock.Mock()
    repo.session.execute.return_value.unique.return_value.one.side_effect = NoResultFound()

    order = await repo.get_order_by_id(1)

    assert order is None


@patch("app.api.v1.ordering.repositories.order_repository.logger")
@patch("app.api.v1.ordering.repositories.order_repository.select")
@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_get_order_by_id_throws_DBError_when_one_throws_Exception(base_repo, select, logger):

    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.return_value = mock.Mock()
    repo.session.execute.return_value.unique.return_value.one.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.get_order_by_id(1)

        logger.exception.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.logger")
@patch("app.api.v1.ordering.repositories.order_repository.select")
@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_get_order_by_id_throws_DBError_when_execute_throws_Exception(
    base_repo, select, logger
):

    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.get_order_by_id(1)

        logger.exception.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.OrderItem")
@patch("app.api.v1.ordering.repositories.order_repository.Order")
@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_create_order_returns_Order_when_successful(base_repo, order_model, order_item_model):

    mock_order_in_db = mock.MagicMock()
    list_of_order_items = [mock.MagicMock(), mock.MagicMock()]
    mock_order_items = list_of_order_items

    mock_order_item = mock.Mock(spec=OrderItem)
    mock_order_item.return_value = mock_order_item

    mock_order = mock.Mock(spec=Order)
    order_model.return_value = mock_order

    repo = OrderRepository()
    repo.session = mock.AsyncMock()

    order = await repo.create_order(mock_order_in_db, mock_order_items)

    assert order_item_model.call_count == len(list_of_order_items)
    repo.session.add.assert_called_once_with(mock_order)
    repo.session.flush.assert_called_once()
    repo.session.refresh.assert_called_once()
    assert isinstance(order, Order)


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_create_order_throws_DBError_when_flush_raises_Exception(base_repo):

    mock_order = mock.MagicMock()
    mock_order_items = [mock.MagicMock()]
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.flush.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.create_order(mock_order, mock_order_items)

    repo.session.add.assert_called_once()
    repo.session.flush.assert_called_once()
    repo.session.refresh.assert_not_called()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_create_order_throws_DBError_when_refresh_raises_Exception(base_repo):

    mock_order = mock.MagicMock()
    mock_order_items = [mock.MagicMock()]
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.refresh.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.create_order(mock_order, mock_order_items)

    repo.session.add.assert_called_once()
    repo.session.flush.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_cancel_order_throws_DBError_when_refresh_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.refresh.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.cancel_order(mock_order)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_cancel_order_throws_DBError_when_execute_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.cancel_order(mock_order)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_not_called()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_cancel_order_returns_cancelled_order_when_successful(base_repo):

    mock_order = mock.Mock(spec=Order)
    cancelled_order = mock_order
    cancelled_order.status = OrderStatus.CANCELLED
    cancelled_order.updated_at = datetime.now()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.side_effect = lambda x: cancelled_order

    order = await repo.cancel_order(mock_order)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()

    assert order.status == OrderStatus.CANCELLED
    assert order.updated_at is not None


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_status_throws_DBError_when_refresh_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.refresh.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.update_status(mock_order, OrderStatus.CANCELLED)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_status_throws_DBError_when_execute_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.update_status(mock_order, OrderStatus.CONFIRMED)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_not_called()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_status_returns_cancelled_order_when_successful(base_repo):

    mock_order = mock.Mock(spec=Order)
    cancelled_order = mock_order
    cancelled_order.status = OrderStatus.CANCELLED
    cancelled_order.updated_at = datetime.now()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.side_effect = lambda x: cancelled_order

    order = await repo.update_status(mock_order, OrderStatus.CANCELLED)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()

    assert order.status == OrderStatus.CANCELLED
    assert order.updated_at is not None


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_associate_throws_DBError_when_refresh_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.refresh.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.update_associate(mock_order, 1)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_associate_throws_DBError_when_execute_raises_Exception(base_repo):

    mock_order = mock.Mock()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.execute.side_effect = Exception()

    with pytest.raises(DBError):
        order = await repo.update_associate(mock_order, 1)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_not_called()


@patch("app.api.v1.ordering.repositories.order_repository.BaseRepository.__init__")
@pytest.mark.asyncio
async def test_order_repository_update_associate_returns_cancelled_order_when_successful(base_repo):

    new_associate = 1
    mock_order = mock.Mock(spec=Order)
    updated_order = mock_order
    updated_order.current_associate = new_associate
    updated_order.updated_at = datetime.now()
    repo = OrderRepository()
    repo.session = mock.AsyncMock()
    repo.session.side_effect = lambda x: updated_order

    order = await repo.update_status(mock_order, new_associate)

    repo.session.execute.assert_called_once()
    repo.session.refresh.assert_called_once()

    assert order.current_associate == new_associate
    assert order.updated_at is not None
