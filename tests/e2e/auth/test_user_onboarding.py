import pytest

AUTH_PREFIX = "/v1/auth/"


@pytest.mark.asyncio
async def test_admin_can_onboard_new_user(admin_user, new_pharmacy_user, dummy_file):

    response = await admin_user.post(AUTH_PREFIX, data=new_pharmacy_user, files={"id_proof_image": dummy_file})

    print(response.json())

    assert response.status_code == 201


@pytest.mark.asyncio
async def test_admin_onboard_new_user_with_same_number_under_same_partner_throws_410(
    admin_user, new_pharmacy_user, dummy_file
):

    response = await admin_user.post(AUTH_PREFIX, data=new_pharmacy_user, files={"id_proof_image": dummy_file})

    response = await admin_user.post(AUTH_PREFIX, data=new_pharmacy_user, files={"id_proof_image": dummy_file})

    assert response.status_code == 410


@pytest.mark.asyncio
async def test_admin_onboard_new_user_with_same_number_under_different_partner_returns_201(
    admin_user, new_pharmacy_user, dummy_file
):

    response = await admin_user.post(AUTH_PREFIX, data=new_pharmacy_user, files={"id_proof_image": dummy_file})

    new_pharmacy_user["partner_code"] = 3
    new_pharmacy_user["location_id"] = 3
    response = await admin_user.post(AUTH_PREFIX, data=new_pharmacy_user, files={"id_proof_image": dummy_file})

    assert response.status_code == 201
