import pytest

from app.api.v1.ordering.endpoints.order import (
    ORDERING_ORDER_ID,
    ORDERING_ROOT,
    ORDERING_CANCELLATION,
    ORDERING_ALLOCATION,
    ORDERING_CONFIRMATION,
)

ORDERING_PREFIX = "/v1/order"


@pytest.mark.asyncio
async def test_sales_associate_can_place_order_for_a_non_existent_customer(
    sales_team_1_associate_1, new_customer_new_order
):

    response = await sales_team_1_associate_1.post(ORDERING_PREFIX + ORDERING_ROOT, data=new_customer_new_order)

    print("############################################################################")
    print(response.json())
    print("############################################################################")

    assert response.status_code == 201


@pytest.mark.asyncio
async def test_sales_associate_cannot_allocate_order(sales_team_1_associate_1):

    response = await sales_team_1_associate_1.put(ORDERING_PREFIX + ORDERING_ALLOCATION.format(order_id=123))

    print(response.json())

    assert response.status_code == 401
