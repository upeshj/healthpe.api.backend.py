import pytest
from app.api.v1.ordering.endpoints.order import (
    ORDERING_ORDER_ID,
    ORDERING_ROOT,
    ORDERING_CANCELLATION,
    ORDERING_ALLOCATION,
    ORDERING_CONFIRMATION,
)

ORDERING_PREFIX = "/v1/order"


@pytest.mark.asyncio
async def test_pharmacy_associate_cannot_place_order(ph_1_associate_1):

    response = await ph_1_associate_1.post(ORDERING_PREFIX + ORDERING_ROOT)

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_pharmacy_associate_cannot_cancel_order(ph_1_associate_1):

    response = await ph_1_associate_1.put(ORDERING_PREFIX + ORDERING_CANCELLATION.format(order_id=123))

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_pharmacy_associate_cannot_allocate_order(ph_1_associate_1):

    response = await ph_1_associate_1.put(ORDERING_PREFIX + ORDERING_ALLOCATION.format(order_id=123))

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_pharmacy_associate_cannot_confirm_order(ph_1_associate_1):

    response = await ph_1_associate_1.put(ORDERING_PREFIX + ORDERING_CONFIRMATION.format(order_id=123))

    assert response.status_code == 401


@pytest.mark.asyncio
async def test_pharmacy_associate_cannot_see_order(ph_1_associate_1):

    response = await ph_1_associate_1.get(ORDERING_PREFIX + ORDERING_ORDER_ID.format(order_id=123))

    assert response.status_code == 401
