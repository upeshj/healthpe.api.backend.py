import pytest
import random, string
from httpx import AsyncClient
from app.main import app

BASE_URL = "http://localhost:8000/"
PARTNER_PREFIX = "v1/partner/"


@pytest.mark.asyncio
async def test_root_partner_returns_status_200(test_app):

    response = await test_app.get(BASE_URL + PARTNER_PREFIX)
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_partner_unauthorized_register_returns_401(test_app, new_partner):

    partner_code = "".join(random.choices(string.ascii_uppercase, k=10))

    response = await test_app.post(
        BASE_URL + PARTNER_PREFIX,
        json=new_partner,
    )
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_partner_authorized_register_returns_201(admin_user, new_partner):

    response = await admin_user.post(
        BASE_URL + PARTNER_PREFIX,
        json=new_partner,
    )
    print(f"Response: {response.json()}")
    assert response.status_code == 201
