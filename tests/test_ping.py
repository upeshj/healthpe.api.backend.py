import pytest


@pytest.mark.asyncio
async def test_ping(test_app):

    response = await test_app.get("/")

    print(response.json())
    assert response.status_code == 200
