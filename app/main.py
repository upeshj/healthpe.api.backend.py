import logging
import logging.config
from fastapi import FastAPI
from app.config import settings
from fastapi.middleware.cors import CORSMiddleware
from app.core.middleware.middleware_request_timing import RequestTimeMiddleware
from .api.v1.router import api_router

logging.config.fileConfig("logging.conf", disable_existing_loggers=False)

logger = logging.getLogger(__name__)

app = FastAPI(title="HealthPe Internal API")

app.add_middleware(RequestTimeMiddleware)
allowed_origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=allowed_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
),
app.include_router(api_router, prefix="/v1")


@app.get("/")
async def ping():

    return {"ping": "pong", "settings": settings}
