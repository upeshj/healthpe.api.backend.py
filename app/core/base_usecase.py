import logging
from abc import ABC, abstractmethod
from fastapi import Request, Response, Depends
from app.core.deps import session_context, get_audit_trail_database
from app.core.audit.repositories import AuditTrailRepository

logger = logging.getLogger(__name__)


class BaseUseCase(ABC):
    # def __init__(self, req: Request, res: Response, audit_session=Depends(get_audit_trail_database)) -> None:
    def __init__(self, req: Request, res: Response, audit: AuditTrailRepository = AuditTrailRepository()) -> None:
        super().__init__()
        self.req = req
        self.res = res
        # self.audit_session = audit_session
        # self.audit = audit

    @abstractmethod
    def execute(self):

        pass

    async def handle_request(self):

        # Something before
        # audit = AuditTrailRepository(self.audit_session)
        # await audit.create_record(self.req)
        # await self.audit.create_record(self.req)

        result = await self.execute()

        session = session_context.get()
        await session.commit()
        logger.info(f" Session id {id(session)} Commited now")
        # Something after

        return result

    # async def _create_audit_trail(self):
