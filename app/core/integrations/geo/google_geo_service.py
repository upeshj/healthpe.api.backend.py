import httpx
import json
import logging
from typing import Tuple
from .geo_service_provider import GeoServiceProvider
from app.config import settings
from .errors import GeoCodingError

logger = logging.getLogger(__name__)

ROUNDING = 7

SAMPLE_RESPONSE = """
{
    "results": [
        {
            "address_components": [
                {
                    "long_name": "Bengaluru",
                    "short_name": "Bengaluru",
                    "types": [
                        "locality",
                        "political"
                    ]
                },
                {
                    "long_name": "Bangalore Urban",
                    "short_name": "Bangalore Urban",
                    "types": [
                        "administrative_area_level_2",
                        "political"
                    ]
                },
                {
                    "long_name": "Karnataka",
                    "short_name": "KA",
                    "types": [
                        "administrative_area_level_1",
                        "political"
                    ]
                },
                {
                    "long_name": "India",
                    "short_name": "IN",
                    "types": [
                        "country",
                        "political"
                    ]
                }
            ],
            "formatted_address": "Bengaluru, Karnataka, India",
            "geometry": {
                "bounds": {
                    "northeast": {
                        "lat": 13.173706,
                        "lng": 77.8826809
                    },
                    "southwest": {
                        "lat": 12.7342888,
                        "lng": 77.3791981
                    }
                },
                "location": {
                    "lat": 12.9715987,
                    "lng": 77.5945627
                },
                "location_type": "APPROXIMATE",
                "viewport": {
                    "northeast": {
                        "lat": 13.173706,
                        "lng": 77.8826809
                    },
                    "southwest": {
                        "lat": 12.7342888,
                        "lng": 77.3791981
                    }
                }
            },
            "place_id": "ChIJbU60yXAWrjsR4E9-UejD3_g",
            "types": [
                "locality",
                "political"
            ]
        }
    ],
    "error_message": "Some error message",
    "status": "OK"
}
"""


class GoogleGeoService(GeoServiceProvider):
    def __init__(self):

        super().__init__()
        self.API_KEY = settings.GOOGLE_GEOCODING_API_KEY
        self.GEOCODING_URL = settings.GOOGLE_GEOCODING_URL

    async def get_long_lat_for_address(self, address: str) -> Tuple[float, float]:
        """
        Retrieves longitude and latitude from Google for a given address.

        Args:
            address (str): Full Address for which long lat is expected.

        Returns:
            Tuple[float, float]: (longitude, latitude) tuple
        """

        url = self.GEOCODING_URL + f"?address={address}&key={self.API_KEY}"

        async with httpx.AsyncClient() as client:

            # response = await client.get(url)
            response = json.loads(SAMPLE_RESPONSE)

            # (longitude, latitude) = self._parse_geocoding_response(response.json())
            (longitude, latitude) = self._parse_geocoding_response(response)

        return (longitude, latitude)

    async def get_address_from_long_lat(self, longitude: float, latitude: float) -> str:

        return ""

    def _parse_geocoding_response(self, response: dict) -> Tuple[float, float]:
        """
        We are expecting exactly one location being returned by Google here.
        If there is more than one location, an exception is raised. The user
        will need to correct the address and send the file again.

        Args:
            response (dict): Response JSON as received from Google Maps API.

        Raises:
            GeoCodingError: [description]

        Returns:
            Tuple[float, float]: Tuple of (longitude, latitude)
        """

        logging.info(f"Response from google: {response}")

        status = response.get("status", None)

        if status in [None, "ZERO_RESULTS"]:
            logger.error(f"{response['error_message']}")
            raise GeoCodingError(code=status, message="No results found for the address")

        if status in ["OVER_QUERY_LIMIT", "OVER_DAILY_LIMIT", "REQUEST_DENIED"]:
            logger.error(f"{response['error_message']}")
            raise GeoCodingError(code=status, message="Check the Google API Key for quota")

        if status in ["INVALID_REQUEST", "UNKNOWN_ERROR"]:
            logger.error(f"{response['error_message']}")
            raise GeoCodingError(
                code=status,
                message="Check the input request if it matches with what Google expects. Check https://developers.google.com/maps/documentation/geocoding/overview",
            )

        if status == "OK":
            results = response.get("results", [])

            if len(results) != 1:
                # Raise proper exception
                logger.error(f"Number of responses recevied is not 1. See response: {response}")
                raise GeoCodingError(
                    code="UNEXPECTED_RESPONSES",
                    message="Should have received single response. Got multiple. Try changing the address.",
                )

            longitude = round(results[0]["geometry"]["location"]["lng"], ROUNDING)
            latitude = round(results[0]["geometry"]["location"]["lat"], ROUNDING)

            return (longitude, latitude)
