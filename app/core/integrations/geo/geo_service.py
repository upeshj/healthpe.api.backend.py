from typing import Tuple
from abc import ABC
from .geo_service_provider import GeoServiceProvider


class GeoService(ABC):
    def __init__(self, geo_service_provider: GeoServiceProvider):

        self.geo_service_provider = geo_service_provider

    async def get_long_lat_for_address(self, address: str) -> Tuple[float, float]:

        return await self.geo_service_provider.get_long_lat_for_address(address)

    async def get_address_from_long_lat(self, longitude: float, latitude: float) -> str:

        return await self.geo_service_provider.get_address_from_long_lat(longitude, latitude)

    def __call__(self):

        return self
