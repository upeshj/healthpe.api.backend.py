class GeoCodingError(Exception):
    def __init__(self, code: str, message: str):

        self.code = code
        self.message = message
        super().__init__(message)

    def __str__(self):
        return f"{self.code} {self.message}"
