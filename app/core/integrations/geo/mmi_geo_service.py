import logging
from devtools import debug
from typing import Tuple
from .geo_service import GeoService

logger = logging.getLogger(__name__)


class MMIGeoService(GeoService):
    def __init__(self):

        super().__init__()

    async def get_long_lat_for_address(self, address: str) -> Tuple[float, float]:

        # TODO: NEed to incorporate google maps logic
        return 77.5946, 12.9755

    async def get_address_from_long_lat(self, longitude: float, latitude: float) -> str:

        return ""
