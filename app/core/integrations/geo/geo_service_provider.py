from typing import Tuple
from abc import ABC, abstractmethod


class GeoServiceProvider(ABC):
    def __init__(self):

        super().__init__()

    @abstractmethod
    async def get_long_lat_for_address(self) -> Tuple[float, float]:

        pass

    @abstractmethod
    async def get_address_from_long_lat(self) -> str:

        pass
