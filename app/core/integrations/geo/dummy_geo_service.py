import random
from typing import Tuple
from .geo_service import GeoService


class DummyGeoService(GeoService):
    def __init__(self):

        pass

    async def get_long_lat_for_address(self, address: str) -> Tuple[float, float]:

        longitude = round(random.uniform(77.59, 77.64), 4)
        latitude = round(random.uniform(12.90, 12.98), 4)

        return longitude, latitude

    async def get_address_from_long_lat(self, longitude: float, latitude: float) -> str:
        return ""
