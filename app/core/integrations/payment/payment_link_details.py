import decimal
from datetime import datetime
from pydantic import BaseModel


class CorePaymentLinkDetails(BaseModel):

    service_provider: str
    hp_reference_no: str
    hp_order_no: int
    creation_time: datetime
    amount: decimal.Decimal
    currency: str
    link_url: str
    link_expiry_time: datetime
