from abc import ABC, abstractmethod
from .payment_link_details import CorePaymentLinkDetails


class PaymentLinkProvider(ABC):
    def __init__(self):

        super().__init__()

    @abstractmethod
    async def generate_payment_link(self) -> CorePaymentLinkDetails:

        pass

    @abstractmethod
    async def get_payment_link_details(self) -> CorePaymentLinkDetails:

        pass
