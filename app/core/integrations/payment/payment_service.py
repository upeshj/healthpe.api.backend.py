from .payment_link_details import CorePaymentLinkDetails
from .payment_link_provider import PaymentLinkProvider


class PaymentService:
    def __init__(self, payment_link_provider: PaymentLinkProvider):

        self.payment_link_provider = payment_link_provider

    async def generate_payment_link(self) -> CorePaymentLinkDetails:

        return await self.payment_link_provider.generate_payment_link()

    def __call__(self):

        return self
