import logging
import boto3
import os
from abc import ABC, abstractmethod
from botocore.exceptions import ClientError
from .errors import StorageServiceError
from .storage_service import StorageService

logger = logging.getLogger(__name__)


class S3StorageService(ABC):
    def __init__(self, bucket_name: str):

        super().__init__()

        self.s3_client = boto3.client("s3")
        self.bucket_name = bucket_name

    # @abstractmethod
    async def save_file(self, file_name: str, content_type: str = "image/jpeg") -> str:

        logger.info(f"Uploading file {file_name} to {self.bucket_name}")

        return await self.upload_file(file_name, content_type)

        # raise NotImplementedError("This method should be implemented in child class")

    async def upload_file(self, file_name: str, content_type: str) -> None:

        try:
            self.s3_client.upload_file(
                file_name,
                self.bucket_name,
                os.path.split(file_name)[-1],
                ExtraArgs={"ServerSideEncryption": "aws:kms", "ContentType": content_type},
            )

        except Exception as e:
            logger.exception(f"S3StorageError:{str(e)}", exc_info=True)
            raise StorageServiceError("", str(e))

    def get_presigned_link(self, object_name: str) -> str:

        presigned_link = self.s3_client.generate_presigned_url(
            "get_object",
            Params={"Bucket": self.bucket_name, "Key": object_name},
            ExpiresIn=86400,
        )

        return presigned_link
