from typing import Protocol


class StorageService(Protocol):
    async def save_file(self, file_path: str, file_name: str) -> str:
        """
        Stores the file to a certain location and returns the path

        Args:
            file_path (str): File path including name

        Returns:
            str: Filepath where the file was stored including filename/objectname
        """

        ...
