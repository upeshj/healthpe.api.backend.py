from .msg91_sms_provider import MSG91SMSProvider
from .otp_type import OTPType
from devtools import debug


class SMSOtp(OTPType):
    def __init__(self, phone_number: str, otp: str):

        self.phone_number = phone_number
        self.otp = otp

        self.sms_provider = MSG91SMSProvider()

    async def send(self) -> None:

        await self.sms_provider.send(self.phone_number, self.otp)
