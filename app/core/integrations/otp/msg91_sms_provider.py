import logging
import requests
from .errors import OTPServiceError, SMSServiceError
from app.config import settings

logger = logging.getLogger(__name__)


class MSG91SMSProvider:
    def __init__(self):

        pass

    async def send(self, phone_number: str, otp: int):

        logger.info(f"Will send SMS {otp} to {phone_number}")

        url = f"{settings.MSG91_OTP_URL}?invisible=1&authkey={settings.MSG91_AUTH_KEY}&mobile={phone_number}&otp={otp}&template_id={settings.MSG91_OTP_TEMPLATE_ID}"

        # TODO:
        """
        MSG91 doesn't appear to work with httpx
        For the time being, we are using requests which is a sync call and blocking.
        However, we will move it to httpx or some other async library
        Check Github: https://github.com/encode/httpx/issues/1942
        """
        # response = httpx.post(url, headers=headers)
        response = requests.post(url)

        status_code = response.status_code

        if status_code != 200:

            logger.error(f"MSG91 returned error: {response.text}")
            raise OTPServiceError(code=str(status_code), message=response.text)

        response_json = response.json()
        response_type = response_json.get("type", "")

        if response_type == "success":
            logger.info(f"MSG91 request id: {response_json.get('request_id', '')}")
            return

        if response_type == "":
            raise OTPServiceError(code=str(status_code), message="Invalid response type from MSG91")

        if response_type == "error":

            error_message = response_json.get("message", response_json.get("msg", ""))
            logger.error(f"MSG91 responded with error: {error_message}")
            raise OTPServiceError(code=str(status_code), message=error_message)

    async def send_sms(self, phone_number: str, message: str):

        url = f"{settings.MSG91_SMS_URL}?invisible=1&authkey={settings.MSG91_SMS_AUTH_KEY}"

        response = requests.post(url)

        status_code = response.status_code

        if status_code == 200:
            logger.info(f"SMS Sent. Request ID: ")
            return

        raise SMSServiceError(code=status_code, message=response.text)
