from otp_type import OTPType
from devtools import debug


class EmailOTP(OTPType):
    def __init__(self, email: str, otp: str):

        self.email = email
        self.otp = otp

    async def send(self) -> None:

        debug(f"Will send Email OTP to {self.email}")
