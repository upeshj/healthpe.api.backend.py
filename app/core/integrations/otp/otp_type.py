from abc import ABC, abstractmethod


class OTPType(ABC):
    @abstractmethod
    async def send(self):

        pass
