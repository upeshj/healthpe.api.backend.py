from .msg91_sms_provider import MSG91SMSProvider


class SMSService:
    def __init__(self):

        self.sms_provider = MSG91SMSProvider()

    async def send(self, phone_number: str, message: str) -> None:

        await self.sms_provider.send_sms(phone_number, message)
