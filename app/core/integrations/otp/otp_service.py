from abc import ABC, abstractmethod
from .otp_type import OTPType


class OTPService:
    def __init__(self):

        pass

    async def send_otp(self, otp_type: OTPType) -> None:

        await otp_type.send()
