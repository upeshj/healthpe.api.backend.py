from datetime import datetime
from sqlalchemy import Column, DateTime
from .base import Base


class ControlFieldsMixin:

    created_at = Column(DateTime, default=datetime.now, nullable=False)
    updated_at = Column(DateTime, nullable=True)


class HealthPeModelBase(Base, ControlFieldsMixin):

    __abstract__ = True
