import re

# Converts a None string to blank
conv = lambda i: i or ""

# Converts all multi spaces to single. For example, "a   b" will return "a b"
single_space = lambda i: re.sub(" +", " ", i)

# Joins all inputs strings with the separator and then replaces multiple occurrences of space with single
joined_and_single_spaced = lambda iterable, separator: single_space(separator.join([conv(i) for i in iterable]))
