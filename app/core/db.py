from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from app.config import settings

DB_USER = settings.DB_USER
DB_PASS = settings.DB_PASS
DB_HOST = settings.DB_HOST
DB_PORT = settings.DB_PORT
DB_NAME = settings.DB_NAME
DB_URL = f"postgresql+asyncpg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

engine = create_async_engine(DB_URL)

AsyncSessionLocal = sessionmaker(
    autocommit=False,
    autoflush=False,
    expire_on_commit=False,
    bind=engine,
    class_=AsyncSession,
)

AUDIT_TRAIL_DB_USER = settings.AUDIT_TRAIL_DB_USER
AUDIT_TRAIL_DB_PASS = settings.AUDIT_TRAIL_DB_PASS
AUDIT_TRAIL_DB_HOST = settings.AUDIT_TRAIL_DB_HOST
AUDIT_TRAIL_DB_PORT = settings.AUDIT_TRAIL_DB_PORT
AUDIT_TRAIL_DB_NAME = settings.AUDIT_TRAIL_DB_NAME
AUDIT_TRAIL_DB_URL = f"postgresql+asyncpg://{AUDIT_TRAIL_DB_USER}:{AUDIT_TRAIL_DB_PASS}@{AUDIT_TRAIL_DB_HOST}:{AUDIT_TRAIL_DB_PORT}/{AUDIT_TRAIL_DB_NAME}"

audit_trail_engine = create_async_engine(AUDIT_TRAIL_DB_URL)

AuditTrailSession = sessionmaker(
    autocommit=False,
    autoflush=False,
    expire_on_commit=False,
    bind=audit_trail_engine,
    class_=AsyncSession,
)
