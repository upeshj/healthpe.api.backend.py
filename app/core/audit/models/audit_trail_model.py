from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from app.core.base_model import HealthPeModelBase
from enum import Enum


class AuditTrail(HealthPeModelBase):

    __tablename__ = "audit_trail"

    id = Column(Integer, primary_key=True)
    actor_id = Column(Integer, nullable=False)
    actor_name = Column(String(30), nullable=False)
    action = Column(String(30), nullable=False)
    entity = Column(String(30), nullable=False)
    full_request = Column(String, nullable=True)

    def __str__(self):

        return f"""
            actor_id = {self.actor_id}
            actor_name = {self.actor_name}
            action = {self.action}
            entity = {self.entity}
            full_request = {self.full_request}
        """
