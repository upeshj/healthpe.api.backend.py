import logging
from app.core.base_repository import BaseRepository
from ..models.audit_trail_model import AuditTrail
from ...deps import get_audit_trail_database
from fastapi import Request

logger = logging.getLogger(__name__)

# class AuditTrailRepository:
#     def __init__(self, session):
#         super().__init__()
#         self.session = session

#     async def create_record(self, request: Request) -> None:

#         if not hasattr(request.state, "current_user"):
#             return

#         record = AuditTrail(
#             actor_id=request.state.current_user.id,
#             actor_name=request.state.current_user.username,
#             action="GET",
#             entity="1",
#             full_request="",
#         )
#         self.session.add(record)
#         await self.session.commit()


class AuditTrailRepository:
    def __init__(self):
        super().__init__()
        # self.session = get_audit_trail_database()

    async def create_record(self, request: Request) -> None:

        if not hasattr(request.state, "current_user"):
            return

        record = AuditTrail(
            actor_id=request.state.current_user.id,
            actor_name=request.state.current_user.username,
            action=request.method,
            entity=request.url,
            full_request=await request.body(),
        )
        # self.session.add(record)
        # await self.session.commit()
        logger.debug(f"Audit: {str(record)}")
