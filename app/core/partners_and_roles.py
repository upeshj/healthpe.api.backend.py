from enum import Enum


class PartnerType(str, Enum):

    # pharmacy = "Pharmacy"
    # ordering = "Ordering"
    # shipping = "Shipping"
    fulfillment = "Fulfillment"
    sales = "Sales"
    delivery = "Delivery"
    sales_and_delivery = "Sales and Delivery"
    shipping = "Shipping"
    admin = "Admin"


class RolesEnum(str, Enum):

    manager = "Manager"
    associate = "Associate"
    admin = "Admin"
