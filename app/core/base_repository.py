from sqlalchemy.ext.asyncio import AsyncSession
from .deps import session_context


class BaseRepository:
    def __init__(self):

        self.session = session_context.get()  # type: AsyncSession
