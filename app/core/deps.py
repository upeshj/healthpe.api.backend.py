import logging
import httpx
from devtools import debug
from sqlalchemy.ext.asyncio import AsyncSession
import contextvars
from .db import AsyncSessionLocal, AuditTrailSession

logger = logging.getLogger(__name__)

session_context = contextvars.ContextVar("session")
current_user = contextvars.ContextVar("current_user")


async def get_database():
    async with AsyncSessionLocal() as session:  # type: AsyncSession
        # await session.execute("SET synchronous_commit TO on")

        try:
            session_context.set(session)
            logger.info(f"Session ID: {id(session)} created")
            yield session
        finally:
            await session.close()


async def get_audit_trail_database():
    async with AuditTrailSession() as session:  # type: AsyncSession

        try:
            logger.info(f"Audit Trail Session ID: {id(session)} created")
            yield session
        finally:
            await session.close()


async def get_httpx_client():

    async with httpx.AsyncClient() as client:

        try:
            yield client
        finally:
            await client.aclose()
