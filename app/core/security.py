import jwt
import datetime
import time
import os
import logging
import hashlib
import binascii
import base64
from typing import Optional, List
from pydantic import BaseModel
from fastapi import Request, status, Depends
from fastapi.exceptions import HTTPException
from fastapi.security import OAuth2PasswordBearer

logger = logging.getLogger(__name__)

SECRET = "MY_SECRET"
TOKEN_TYPE = "Bearer"
TOKEN_EXPIRY_IN_SECONDS = 60 * 60 * 24 * 7
HASH_ALGORITHM = "sha256"
ENCODING = "utf-8"

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/login")


class TokenData(BaseModel):

    username: Optional[str] = None
    scopes: List[str] = []


def hash_password(password: str) -> str:

    salt = base64.b64encode(os.urandom(32))
    key = base64.b64encode(hashlib.pbkdf2_hmac(HASH_ALGORITHM, password.encode(ENCODING), salt, 10000))

    hashed_password = key.decode(ENCODING) + ":" + salt.decode(ENCODING)

    # print(
    #     f"""
    #     salt b64encode --> {salt}
    #     salt decoded --> {hashed_password.split(":")[1]}

    #     key b64encode --> {key}
    #     key decoded --> {hashed_password.split(":")[0]}
    # """
    # )

    return hashed_password


def verify_password(plain_password: str, hashed_password: str) -> bool:

    key_from_hashed_password, salt_from_hashed_password = hashed_password.split(":")

    salt = salt_from_hashed_password.encode(ENCODING)
    key = key_from_hashed_password.encode(ENCODING)

    # print(
    #     f"""
    #     salt from hashed_password --> {salt_from_hashed_password}
    #     salt decoded --> {salt}

    #     key from hashed_password --> {key_from_hashed_password}

    #     key decoded --> {key}
    # """
    # )

    new_key = base64.b64encode(hashlib.pbkdf2_hmac(HASH_ALGORITHM, plain_password.encode(ENCODING), salt, 10000))

    print(f"New key: {new_key}")

    return new_key == key


def get_token_data(token: str = Depends(oauth2_scheme)) -> TokenData:

    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    payload = jwt.decode(token, SECRET, algorithms=["HS256"])
    username: str = payload.get("sub")
    if username is None or username == "":
        raise credentials_exception
    token_data = TokenData(username=username)

    return token_data


def must_be_logged_in(req: Request) -> str:

    token = req.headers.get("Authorization", "")

    if token == "":
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="You must be logged in.")

    current_user = validate_token(token)

    if not current_user or current_user == "":
        raise HTTPException(status.HTTP_401_UNAUTHORIZED)

    return current_user


def create_jwt_token(payload: dict) -> str:

    to_encode = payload.copy()
    to_encode["exp"] = datetime.datetime.utcnow() + datetime.timedelta(seconds=TOKEN_EXPIRY_IN_SECONDS)
    to_encode["iat"] = datetime.datetime.utcnow()

    return jwt.encode(to_encode, SECRET, algorithm="HS256")


def validate_token(token: str) -> str:

    try:
        token_type, token = token.split()

        if token_type != "Bearer":
            raise jwt.InvalidTokenError()

        payload = jwt.decode(token, SECRET, algorithms="HS256")
    except ValueError as exc:
        raise jwt.InvalidTokenError()
    except jwt.ExpiredSignatureError as exc:
        return ""
    except jwt.InvalidTokenError as exc:
        return ""

    return payload["sub"]
