import logging
from app.core.integrations.storage.s3_storage_service import S3StorageService
from app.config import settings

logger = logging.getLogger(__name__)


class VendorInvoiceStorageService(S3StorageService):
    def __init__(self):
        super().__init__(settings.VENDOR_INVOICE_BUCKET_NAME)
