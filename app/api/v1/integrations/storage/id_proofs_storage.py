import logging
import boto3
from botocore.exceptions import ClientError
from app.core.integrations.storage.errors import StorageServiceError
from app.core.integrations.storage.s3_storage_service import S3StorageService
from app.config import settings

logger = logging.getLogger(__name__)


class IDProofsStorageService(S3StorageService):
    def __init__(self):
        super().__init__(settings.ID_PROOFS_BUCKET_NAME)
