import logging
from fastapi import Depends, Request, Response
from .usecase_ordering_base import BaseOrderingUseCase
from ..repositories.order_repository import OrderRepository
from .observers.order_confirmation_notification_observer import OrderConfirmationNotificationObserver
from ..models.order_model import OrderStatus
from ..schemas.order_schema import DeliverOrder

logger = logging.getLogger(__name__)


class DeliverOrderUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_to_deliver: DeliverOrder,
        order_repo: OrderRepository = Depends(),
    ):

        super().__init__(
            req=req,
            res=res,
            order_id=order_to_deliver.id,
            order_repo=order_repo,
        )

        self.order_to_deliver = order_to_deliver
        self.order_repo = order_repo
        # self.order_state.add_observer(FulfillmentConfirmationObserver(self.order_to_deliver.fulfillment_requests))
        # self.order_state.add_observer(OrderConfirmationNotificationObserver())

    async def do_execute(self):

        delivered_order = await self.order_repo.update_status(self.order, OrderStatus.DELIVERED)

        return delivered_order
