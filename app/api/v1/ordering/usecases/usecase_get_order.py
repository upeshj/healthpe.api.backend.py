import logging
import json
import httpx
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from pydantic.error_wrappers import ValidationError
from app.api.v1.customer.schemas.customer_address_schema import CustomerAddressInDB, CustomerAddressOut
from app.config import settings
from app.api.v1.customer.schemas.customer_schema import CustomerOut
from ..schemas.order_item_schema import OrderItemInDB
from app.core.deps import get_httpx_client
from .....core.base_usecase import BaseUseCase
from ..schemas.order_schema import CreateOrder, OrderInDB
from ..repositories.order_repository import OrderRepository
from ...customer.repositories.customer_repository import CustomerRepository
from ...customer.repositories.customer_address_repository import CustomerAddressRepository
from ..models.order_model import OrderStatus
from ..models.order_item_model import OrderItemStatus
from ...integrations.storage.id_proofs_storage import IDProofsStorageService

logger = logging.getLogger(__name__)


class GetOrderUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_id: int,
        order_repository: OrderRepository = Depends(),
    ) -> None:

        super().__init__(req, res)

        self.order_id = order_id
        self.order_repository = order_repository

    async def execute(self):

        # Check if customer exists. If not, then create
        # customer = await self.customer_repository.get_customer_by_phone(self.order.customer.phone_number)
        order = await self.order_repository.get_order_by_id(self.order_id)

        if not order:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"Order {self.order_id} not found")

        return order
