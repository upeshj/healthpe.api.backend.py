import logging
from fastapi import Depends, Request, Response
from .usecase_ordering_base import BaseOrderingUseCase
from ..repositories.order_repository import OrderRepository
from ..schemas.order_allocation_schema import OrderAllocationSchema

logger = logging.getLogger(__name__)


class AllocateOrderUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_id: int,
        order_allocation: OrderAllocationSchema,
        order_repo: OrderRepository = Depends(),
    ):

        super().__init__(
            req=req,
            res=res,
            order_id=order_id,
            order_repo=order_repo,
        )

        self.order_repo = order_repo
        self.order_allocation = order_allocation

    async def do_execute(self):

        updated_order = await self.order_repo.update_associate(self.order, self.order_allocation.new_associate)

        return updated_order
