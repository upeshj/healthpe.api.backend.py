import logging
import json
import tempfile
import os

from app.config import settings
from datetime import datetime, timedelta
from pydantic.error_wrappers import ValidationError
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status, File, UploadFile

from app.core.base_usecase import BaseUseCase
from app.core.integrations.storage.errors import StorageServiceError
from ..models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus
from ..schemas.fulfillment.fulfillment_proposal_schema import FulfillmentProposalBase
from ..schemas.fulfillment.fulfillment_packed_schema import PackedFulfillmentSchema
from ..repositories.fulfillment.fulfillment_request_repository import FulfillmentRequestRepository
from ...integrations.storage.vendor_invoice_storage import VendorInvoiceStorageService

logger = logging.getLogger(__name__)


class FulfillmentReadyForPickupUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        # TODO: The packed_fulfillment field needs to be a PackedFulfillmentSchema object. However, due to FastAPI/pydantic
        # Limitation, we are keeping it as str here and later convert it to PackedFulfillmentSchema object in
        # __init__. There are many other solutions possible but it seems they will need to be done
        # recursively for every object inside order.
        # Check this link for reference: https://stackoverflow.com/questions/60127234/how-to-use-a-pydantic-model-with-form-data-in-fastapi
        packed_fulfillment: str = Form(...),
        invoice_file: UploadFile = File(...),
        fulfillment_request_repo: FulfillmentRequestRepository = Depends(),
        vendor_invoice_storage: VendorInvoiceStorageService = Depends(),
    ):
        super().__init__(req, res)

        try:
            self.packed_fulfillment = PackedFulfillmentSchema(**json.loads(packed_fulfillment))
        except ValidationError as e:
            raise HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY, detail=e.errors())

        self.invoice_file = invoice_file
        self.vendor_invoice_storage = vendor_invoice_storage
        self.fulfillment_request_repo = fulfillment_request_repo

    async def execute(self):

        logger.debug(f"Packed Fulfillment: {self.packed_fulfillment}")
        invoice_file_name = await self._save_invoice()

        logger.debug(f"Invoice uploaded with filename: {invoice_file_name}")
        self.packed_fulfillment.invoice_file = invoice_file_name
        self.packed_fulfillment.status = FulfillmentRequestStatus.ReadyForPickup

        await self.fulfillment_request_repo.update_invoice_file_and_status(self.packed_fulfillment)

        # TODO: Make a ledger entry
        # TODO: Need to mark the order ready for Pickup and that should flow to history

    async def _save_invoice(self) -> str:
        """
        This method will save the invoice to local temp, then upload to cloud storage and then cleanup the
        local file. We have not optimized the S3 upload since the volume will be very little for this activity.
        Once we see the need to scale, we can upload the file to S3 as an offline task so that the response
        time to users is not compromised.

        Raises:
            HTTPException: [description]

        Returns:
            str: The filename without directory. This filename will be the same that can be seen on cloud storage in
            the respective bucket. The object and bucket are private but to access, use the get_presigned_link
            method on StorageService.
        """

        invoice_file_name = f"{self.req.state.current_user.partner_code}_{self.req.state.current_user.location_id}_{self.req.state.current_user.id}_{self.invoice_file.filename}"

        full_file_path = os.path.join(tempfile.gettempdir(), invoice_file_name)
        with open(full_file_path, "wb") as output_file:
            output_file.write(await self.invoice_file.read())

        try:
            await self.vendor_invoice_storage.save_file(full_file_path, self.invoice_file.content_type)

            # Cleanup the local storage
            os.remove(full_file_path)

        except StorageServiceError as e:
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "Error occured while trying to upload invoice. Please try again.",
            )

        return invoice_file_name
