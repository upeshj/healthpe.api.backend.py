import logging
from app.api.v1.ordering.models.order_model import Order

from app.config import settings
from datetime import datetime, timedelta
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status

from ..usecases.usecase_ordering_base import BaseOrderingUseCase
from ..schemas.fulfillment.fulfillment_request_schema import CreateFulfillmentRequest, FulfillmentRequestInDB
from ..repositories.fulfillment.fulfillment_request_repository import FulfillmentRequestRepository
from ..repositories.order_repository import OrderRepository
from ..models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus
from ..models.order_model import OrderStatus

logger = logging.getLogger(__name__)


class CreateFulfillmentRequestUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        fulfillment_request: CreateFulfillmentRequest,
        order_repo: OrderRepository = Depends(),
        fulfillment_request_repo: FulfillmentRequestRepository = Depends(),
    ):
        super().__init__(req, res, fulfillment_request.order_id, order_repo)
        self.fulfillment_request = fulfillment_request
        self.fulfillment_request_repo = fulfillment_request_repo

    async def do_execute(self):

        fulfillments = []
        expiration_time = datetime.now() + timedelta(minutes=settings.FULFILLMENT_REQUEST_TIMEOUT)
        for partner_location in self.fulfillment_request.partner_locations:

            fulfillment_in_db = FulfillmentRequestInDB(
                created_by=self.req.state.current_user.id,
                order_id=self.fulfillment_request.order_id,
                partner_location_id=partner_location,
                status=FulfillmentRequestStatus.Open,
                expiration_time=expiration_time,
                items=self.order.items,
            )
            fulfillments.append(await self.fulfillment_request_repo.create_fulfillment(fulfillment_in_db))

        updated_order = await self.order_repository.update_status(self.order, OrderStatus.OPEN)
        # updated_order.fulfillments = fulfillments

        return self.order
