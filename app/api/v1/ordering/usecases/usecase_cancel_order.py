import logging
from fastapi import Depends, Request, Response
from .usecase_ordering_base import BaseOrderingUseCase
from ..repositories.order_repository import OrderRepository
from .observers.order_cancellation_notification_observer import OrderCancellationNotificationObserver

logger = logging.getLogger(__name__)


class CancelOrderUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_id: int,
        order_repo: OrderRepository = Depends(),
    ):

        super().__init__(
            req=req,
            res=res,
            order_id=order_id,
            order_repo=order_repo,
        )

        self.order_repo = order_repo
        self.order_state.add_observer(OrderCancellationNotificationObserver())

    async def do_execute(self):

        cancelled_order = await self.order_repo.cancel_order(self.order)

        return cancelled_order
