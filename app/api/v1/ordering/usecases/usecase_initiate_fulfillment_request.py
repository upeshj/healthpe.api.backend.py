# import logging
# from typing import List
# from fastapi import Depends, Request, Response
# from .usecase_ordering_base import BaseOrderingUseCase
# from ..repositories.order_repository import OrderRepository
# from ...partner.schemas.partner_schema import PartnerOut

# logger = logging.getLogger(__name__)


# class InitiateFulfillmentRequestUseCase(BaseOrderingUseCase):
#     def __init__(
#         self,
#         req: Request,
#         res: Response,
#         order_id: int,
#         fulfillment_partners: List[PartnerOut],
#         order_repo: OrderRepository = Depends(),
#     ):

#         super().__init__(
#             req=req,
#             res=res,
#             order_id=order_id,
#             order_repo=order_repo,
#         )

#         self.order_repo = order_repo
#         self.fulfillment_partners = fulfillment_partners

#     async def do_execute(self):

#         updated_order = await self.order_repo.update_associate(self.order, self.order_allocation.new_associate)

#         return updated_order
