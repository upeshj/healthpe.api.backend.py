import logging
from fastapi import Depends, Request, Response
from .usecase_ordering_base import BaseOrderingUseCase
from ..repositories.order_repository import OrderRepository
from .observers.order_confirmation_notification_observer import OrderConfirmationNotificationObserver
from ..models.order_model import OrderStatus
from ..schemas.order_schema import PickedOrder

logger = logging.getLogger(__name__)


class PickedOrderUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        picked_order: PickedOrder,
        order_repo: OrderRepository = Depends(),
    ):

        super().__init__(
            req=req,
            res=res,
            order_id=picked_order.id,
            order_repo=order_repo,
        )

        self.picked_order = picked_order
        self.order_repo = order_repo
        # self.order_state.add_observer(FulfillmentConfirmationObserver(self.order_to_deliver.fulfillment_requests))
        # self.order_state.add_observer(OrderConfirmationNotificationObserver())

    async def do_execute(self):

        delivered_order = await self.order_repo.update_status(self.order, OrderStatus.IN_TRANSIT)

        return delivered_order
