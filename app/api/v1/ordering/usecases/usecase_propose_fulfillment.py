import logging
from app.api.v1.ordering.models.order_model import Order

from app.config import settings
from datetime import datetime, timedelta
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status

from app.core.base_usecase import BaseUseCase
from ..models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus
from ..schemas.fulfillment.fulfillment_proposal_schema import FulfillmentProposalInDB
from ..schemas.fulfillment.fulfillment_proposal_schema import FulfillmentProposalBase
from ..repositories.fulfillment.fulfillment_request_repository import FulfillmentRequestRepository

logger = logging.getLogger(__name__)


class ProposeFulfillmentUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        fulfillment_proposal: FulfillmentProposalBase,
        fulfillment_request_repo: FulfillmentRequestRepository = Depends(),
    ):
        super().__init__(req, res)
        self.fulfillment_proposal = fulfillment_proposal
        self.fulfillment_request_repo = fulfillment_request_repo

    async def execute(self):

        logger.debug(f"Fulfillment proposal: {self.fulfillment_proposal}")

        proposal_in_db = FulfillmentProposalInDB(
            **self.fulfillment_proposal.dict(),
            status=FulfillmentRequestStatus.Proposed,
            proposed_by=self.req.state.current_user.id,
            location_id=self.req.state.current_user.location_id,
        )

        await self.fulfillment_request_repo.set_fulfillment_proposal(proposal_in_db)

        # TODO: Mark order as proposed if the fulfillemnt proposal is complete
