import logging
from fastapi import Depends, Request, Response
from .usecase_ordering_base import BaseOrderingUseCase
from ..repositories.order_repository import OrderRepository
from .observers.order_confirmation_notification_observer import OrderConfirmationNotificationObserver
from .observers.fulfillment_confirmation_observer import FulfillmentConfirmationObserver
from ..models.order_model import OrderStatus
from ..schemas.order_schema import ConfirmOrder

logger = logging.getLogger(__name__)


class ConfirmOrderUseCase(BaseOrderingUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_to_confirm: ConfirmOrder,
        order_repo: OrderRepository = Depends(),
    ):

        super().__init__(
            req=req,
            res=res,
            order_id=order_to_confirm.id,
            order_repo=order_repo,
        )

        self.order_to_confirm = order_to_confirm
        self.order_repo = order_repo
        self.order_state.add_observer(FulfillmentConfirmationObserver(self.order_to_confirm.fulfillment_requests))
        self.order_state.add_observer(OrderConfirmationNotificationObserver())

    async def do_execute(self):

        confirmed_order = await self.order_repo.update_status(self.order, OrderStatus.CONFIRMED)

        return confirmed_order
