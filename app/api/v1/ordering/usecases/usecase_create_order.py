import logging
import json
import httpx
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from pydantic.error_wrappers import ValidationError
from app.api.v1.customer.schemas.customer_address_schema import CustomerAddressInDB, CustomerAddressOut
from app.config import settings
from app.api.v1.customer.schemas.customer_schema import CustomerOut
from ..schemas.order_item_schema import OrderItemInDB
from app.core.deps import get_httpx_client
from .....core.base_usecase import BaseUseCase
from ..schemas.order_schema import CreateOrder, OrderInDB
from ..repositories.order_repository import OrderRepository
from ...customer.repositories.customer_repository import CustomerRepository
from ...customer.repositories.customer_address_repository import CustomerAddressRepository
from ..models.order_model import OrderStatus
from ..models.order_item_model import OrderItemStatus
from ...integrations.storage.id_proofs_storage import IDProofsStorageService

logger = logging.getLogger(__name__)


class CreateOrderUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        # TODO: The order field needs to be a CreateOrder object. However, due to FastAPI/pydantic
        # Limitation, we are keeping it as str here and later convert it to CreateOrder object in
        # __init__. There are many other solutions possible but it seems they will need to be done
        # recursively for every object inside order.
        # Check this link for reference: https://stackoverflow.com/questions/60127234/how-to-use-a-pydantic-model-with-form-data-in-fastapi
        order: str = Form(...),
        order_repository: OrderRepository = Depends(),
        prescription_storage_service: IDProofsStorageService = Depends(),
        customer_repository: CustomerRepository = Depends(),
        customer_service: httpx.AsyncClient = Depends(get_httpx_client),
        # customer_address_repository: CustomerAddressRepository = Depends(),
        # partner_repository: PartnerRepository = Depends(),
        # partner_sourcing_repo: PartnerSourcingRepository = Depends(),
    ) -> None:

        super().__init__(req, res)

        try:
            self.order = CreateOrder(**json.loads(order))
        except ValidationError as e:
            raise HTTPException(status.HTTP_422_UNPROCESSABLE_ENTITY, detail=e.errors())

        self.prescription_storage_service = prescription_storage_service
        self.order_repository = order_repository
        self.customer_repository = customer_repository
        # self.customer_address_repository = customer_address_repository
        self.customer_service = customer_service

    async def execute(self):

        # Check if customer exists. If not, then create
        # customer = await self.customer_repository.get_customer_by_phone(self.order.customer.phone_number)
        customer = await self._get_or_create_customer()

        logger.info(f"Customer {customer}")

        # If customer exists, the check if this address already exists. If not add it
        delivery_address_id = self.order.delivery_address.address_id
        if not delivery_address_id:
            delivery_address = await self._create_address(customer.id)

            delivery_address_id = delivery_address.id

        # Create order record with order items
        order_in_db = OrderInDB(
            creator=self.req.state.current_user.id,
            customer=customer.id,
            current_associate=None,
            status=OrderStatus.PENDING,
            delivery_address_id=delivery_address_id,
        )
        order_items = [
            OrderItemInDB(**order_item.dict(), status=OrderItemStatus.PENDING) for order_item in self.order.items
        ]
        new_order = await self.order_repository.create_order(order_in_db, order_items=order_items)

        return new_order

    async def _create_address(self, customer_id: int):

        # address = await self.customer_address_repository.create_address(
        #     CustomerAddressInDB(**self.order.delivery_address.dict())
        # )

        # return address

        try:
            headers = {"Authorization": self.req.headers.get("Authorization")}
            address = await self.customer_service.post(
                f"{settings.BASE_URL}{settings.CUSTOMER_SERVICE}/{customer_id}/address/",
                json=self.order.delivery_address.dict(),
                headers=headers,
            )

            address = CustomerAddressOut(**address.json())

            return address
        except HTTPException as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal error occurred. Please try again."
            )

    async def _get_or_create_customer(self) -> CustomerOut:

        """
        This method will call the customer service to get the customer details.
        If customer does not exist, it will call the customer service to create
        a new customer and return that.

        # TODO: Need to implement the service discovery mechanism.

        Raises:
            HTTPException

        Returns:
            [CustomerOut]
        """

        try:
            headers = {"Authorization": self.req.headers.get("Authorization")}
            customer = await self.customer_service.get(
                f"{settings.BASE_URL}{settings.CUSTOMER_SERVICE}/{self.order.customer.phone_number}",
                headers=headers,
            )

            if not customer.json():

                logger.info(
                    f"Customer with phone number {self.order.customer.phone_number} does not exist. Creating one."
                )
                customer = await self.customer_service.post(
                    f"{settings.BASE_URL}{settings.CUSTOMER_SERVICE}/",
                    json=self.order.customer.dict(),
                    headers=headers,
                )
        except HTTPException as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal error occurred. Please try again."
            )

        customer = CustomerOut(**customer.json())

        return customer
