from abc import ABC, abstractmethod


class OrderObserver(ABC):
    @abstractmethod
    async def update(self, state):

        pass
