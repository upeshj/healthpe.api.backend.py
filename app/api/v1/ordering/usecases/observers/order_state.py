import copy
from typing import List
from fastapi import Request, Response, Depends
from app.core.base_usecase import BaseUseCase
from .order_observer import OrderObserver

# from ...models.order_model import Order


class OrderState:
    def __init__(self):

        self.observers = []
        self.current_state = None
        self.previous_state = None

    def add_observer(self, observer: OrderObserver):

        self.observers.append(observer)

    async def set_current_state(self, state):

        self.current_state = copy.deepcopy(state)
        for observer in self.observers:
            await observer.update(self)

    async def set_previous_state(self, state):

        self.previous_state = copy.deepcopy(state)

    @property
    def current(self):
        return self.current_state

    @property
    def previous(self):
        return self.previous_state
