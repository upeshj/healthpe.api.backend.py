import logging
from typing import List
from .order_observer import OrderObserver
from ...repositories.fulfillment.fulfillment_request_repository import FulfillmentRequestRepository
from ...schemas.fulfillment.fulfillment_request_schema import FulfillmentRequestOut

logger = logging.getLogger(__name__)


class FulfillmentConfirmationObserver(OrderObserver):
    """
    This listens to Order update for the confirmation flow. When order is confirmed, fulfillment
    proposal also needs to be confirmed so that Pharmacy can take required action.

    It takes in the input the fulfillment requests/proposals which need to be confirmed.
    """

    def __init__(self, fulfillment_requests: List[FulfillmentRequestOut]):

        self.fulfillment_repository = FulfillmentRequestRepository()
        self.fulfillment_requests = fulfillment_requests

    async def update(self, state):

        logger.info(f"Fulfillment Confirmation observer called")

        for fulfillment_request in self.fulfillment_requests:

            logger.debug(f"Confirming fulfillment: {fulfillment_request}")
            await self.fulfillment_repository.confirm_fulfillment_proposal(fulfillment_request)
