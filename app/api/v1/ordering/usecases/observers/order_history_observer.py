import logging
from .order_observer import OrderObserver
from ...models.order_model import Order
from ...repositories.order_history_repository import OrderHistoryRepository
from ...schemas.order_history_schema import OrderHistorySchema

logger = logging.getLogger(__name__)


class OrderHistoryObserver(OrderObserver):
    def __init__(self):

        self.order_history_repo = OrderHistoryRepository()

    async def update(self, state):

        logger.info(f"Order History called")

        previous_state = state.previous  # type: Order
        current_state = state.current  # type: Order

        logger.info(f"Current: {current_state.status}, Previous: {previous_state.status}")

        record = OrderHistorySchema(
            order_id=current_state.id,
            current_associate=current_state.current_associate,
            previous_associate=previous_state.current_associate,
            current_status=current_state.status,
            previous_status=previous_state.status,
        )

        await self.order_history_repo.create_order_history(record)
