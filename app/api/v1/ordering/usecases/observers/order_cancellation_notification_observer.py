import logging
from app.config import settings
from .order_observer import OrderObserver

logger = logging.getLogger(__name__)


class OrderCancellationNotificationObserver(OrderObserver):
    def __init__(self):

        super().__init__()

    async def update(self, state):

        logger.info(f"{self.__class__.__name__} called")

        if settings.NOTIFY_ORDER_CANCELLATION:
            logger.info(f"Order {state.current_state.id} is cancelled. Will send notification to customer.")
