import logging
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from pydantic.error_wrappers import ValidationError
from app.config import settings
from app.core.partners_and_roles import PartnerType, RolesEnum
from ...auth.models.user_model import User
from .....core.base_usecase import BaseUseCase
from ...integrations.storage.id_proofs_storage import IDProofsStorageService
from ..repositories.fulfillment.fulfillment_request_repository import FulfillmentRequestRepository

logger = logging.getLogger(__name__)


class GetFulfillmentsUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        fulfillment_repository: FulfillmentRequestRepository = Depends(),
    ) -> None:

        super().__init__(req, res)

        self.fulfillment_repository = fulfillment_repository

    async def execute(self):

        """
        If Partner -> Fulfillment (Pharmacy), then return only those fulfillments which
                are assigned to that pharmacy (location) where the logged in user works

        If Partner -> Delivery:
            User role: Associate: Show only those fulfillments which belong to the order
                                that this user is currently assigned to.
            User role: Manager: Show all fulfillments

        If Partner -> Sales: Filter only those fulfillments which require payment
            User role: Associate: Show only those fulfillments which belong to the order
                                that this user is currently assigned to.
            User role: Manager: Show all fulfillments
        """

        # TODO: think about a better design here. Cannot have so many if-else

        user = self.req.state.current_user  # type: User

        if user.partner.ptype == PartnerType.delivery:
            if user.role == RolesEnum.associate:
                fulfillments = await self.fulfillment_repository.get_fulfillments_delivery_team_associate(user.id)
                return fulfillments
            elif user.role == RolesEnum.manager:
                fulfillments = await self.fulfillment_repository.get_fulfillments_delivery_team_manager()
                return fulfillments
            else:
                raise HTTPException(
                    status.HTTP_406_NOT_ACCEPTABLE, detail=f"This user is not authorized for query fulfillments."
                )

        if user.partner.ptype == PartnerType.fulfillment:
            return await self.fulfillment_repository.get_fulfillments_fulfillment_team(user.location_id)
