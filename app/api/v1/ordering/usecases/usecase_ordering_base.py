from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from .observers.order_state import OrderState
from .observers.order_history_observer import OrderHistoryObserver
from ..repositories.order_repository import OrderRepository


class BaseOrderingUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_id: int,
        order_repo: OrderRepository,
    ):

        super().__init__(req, res)

        self.order_id = order_id
        self.order_repository = order_repo
        self.order_state = OrderState()
        self.order_state.add_observer(OrderHistoryObserver())

    async def execute(self):

        self.order = await self.order_repository.get_order_by_id(self.order_id)

        if not self.order:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"Order {self.order_id} not found")

        await self.order_state.set_previous_state(self.order)

        updated_order = await self.do_execute()

        # await self.order_state.set_current_state(updated_order)
        await self.order_state.set_current_state(self.order)

        return updated_order
