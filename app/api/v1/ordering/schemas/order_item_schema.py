from pydantic import BaseModel


class BaseOrderItem(BaseModel):

    name: str
    quantity: int

    class Config:
        orm_mode = True


class CreateOrderItem(BaseOrderItem):

    pass


class OrderItemInDB(BaseOrderItem):

    status: str


class OrderItemOut(OrderItemInDB):

    id: int
