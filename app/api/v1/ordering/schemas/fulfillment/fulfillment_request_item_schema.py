import decimal
from pydantic import BaseModel, Field


class FulfillmentRequestItemSchema(BaseModel):

    requested_quantity: int = Field(alias="quantity")
    order_item_id: int = Field(alias="id")
    name: str

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class FulfillmentRequestItemInDB(FulfillmentRequestItemSchema):

    fulfillment_id: int


class FulfillmentRequestItemUpdate(FulfillmentRequestItemSchema):
    fulfillable_quantity: int
    quoted_price: decimal.Decimal
