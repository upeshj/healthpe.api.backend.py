from typing import List, Union
from datetime import datetime
from pydantic import BaseModel, Field
from ...models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus
from ...schemas.fulfillment.fulfillment_request_item_schema import FulfillmentRequestItemSchema
from ...schemas.fulfillment.fulfillment_proposal_item_schema import FulfillmentProposalItemBase


class FulfillmentRequestBase(BaseModel):

    order_id: int


class CreateFulfillmentRequest(FulfillmentRequestBase):
    partner_locations: List[int]


class FulfillmentRequestInDB(FulfillmentRequestBase):

    created_by: int
    status: FulfillmentRequestStatus
    partner_location_id: int
    expiration_time: datetime
    """
    This is Union becasue the fulfillment request can have request populated or not.
    There is scope of improvement here to simplify. # TODO
    """
    items: Union[List[FulfillmentProposalItemBase], List[FulfillmentRequestItemSchema]]

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class FulfillmentRequestOut(FulfillmentRequestInDB):

    id: int = Field(alias="fulfillment_id")
