from typing import List
from pydantic import BaseModel, Field
from .fulfillment_proposal_item_schema import FulfillmentProposalItemBase
from ...models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus


class FulfillmentProposalBase(BaseModel):

    id: int = Field(alias="fulfillment_id")
    items: List[FulfillmentProposalItemBase]

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class CreateFulfillmentProposal(FulfillmentProposalBase):
    pass


class FulfillmentProposalInDB(CreateFulfillmentProposal):

    proposed_by: int
    status: FulfillmentRequestStatus
    location_id: int


class ConfirmFulfillmentProposal(BaseModel):

    id: int = Field(alias="fulfillment_id")
