import decimal
from pydantic import BaseModel, Field


class FulfillmentProposalItemBase(BaseModel):

    id: int
    quantity: int = Field(alias="requested_quantity")
    fulfillable_quantity: int
    name: str
    quoted_price: decimal.Decimal

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
