from typing import Optional
from pydantic import BaseModel, Field


class PackedFulfillmentSchema(BaseModel):

    id: int = Field(alias="fulfillment_id")
    invoice_file: Optional[str] = None
    status: Optional[str] = None
