from datetime import datetime
from typing import List, Optional, Union
from pydantic import BaseModel, validator, Field
from .order_item_schema import CreateOrderItem

from ...customer.schemas.customer_schema import CreateCustomer, CustomerOut
from ...customer.schemas.customer_address_schema import CreateCustomerAddress, OrderDeliveryAddress
from ...auth.schemas.user_schema import UserOut
from ..schemas.order_item_schema import OrderItemOut
from ..schemas.fulfillment.fulfillment_request_schema import FulfillmentRequestOut


class BaseOrder(BaseModel):

    customer: CreateCustomer
    items: List[CreateOrderItem]
    delivery_address: OrderDeliveryAddress  # CreateCustomerAddress

    @validator("items")
    def non_empty_items(cls, v):

        if not v or len(v) == 0:
            raise ValueError("Order Items array cannot be empty")
        return v

    class Config:
        orm_mode = True


class CreateOrder(BaseOrder):

    pass


class OrderInDB(BaseModel):

    creator: int
    customer: int
    current_associate: Optional[int] = None
    status: str
    delivery_address_id: int


class OrderOut(OrderInDB):

    id: int = Field(alias="order_id")
    created_at: datetime
    customer_details: CustomerOut
    creator_details: UserOut
    items: List[OrderItemOut]
    delivery_address: OrderDeliveryAddress
    fulfillment_requests: List[FulfillmentRequestOut]

    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class ConfirmOrder(BaseModel):

    id: int = Field(alias="order_id")
    fulfillment_requests: List[FulfillmentRequestOut]


class DeliverOrder(BaseModel):

    id: int = Field(alias="order_id")
    # fulfillment_requests: List[FulfillmentRequestOut]


class PickedOrder(BaseModel):

    id: int = Field(alias="order_id")
    # fulfillment_requests: List[FulfillmentRequestOut]
