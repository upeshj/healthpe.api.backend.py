from pydantic import BaseModel


class OrderHistorySchema(BaseModel):

    order_id: int
    current_associate: int = None
    previous_associate: int = None
    current_status: str
    previous_status: str
