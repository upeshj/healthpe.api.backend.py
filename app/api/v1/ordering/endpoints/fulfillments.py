from typing import List
from fastapi import APIRouter, Depends, Security
from app.core.deps import get_database
from ...auth.deps import authorize
from ..schemas.order_schema import OrderOut
from ..schemas.fulfillment.fulfillment_request_schema import FulfillmentRequestOut
from ..usecases.usecase_create_fulfillment_request import CreateFulfillmentRequestUseCase
from ..usecases.usecase_get_fulfillments import GetFulfillmentsUseCase
from ..usecases.usecase_propose_fulfillment import ProposeFulfillmentUseCase
from ..usecases.usecase_fulfillment_ready_for_pickup import FulfillmentReadyForPickupUseCase

router = APIRouter()


@router.post(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["createFulfillmentRequest"])],
    response_model=OrderOut,
)
async def create_fulfillment_request(create_fulfillment_usecase: CreateFulfillmentRequestUseCase = Depends()):

    fulfillments = await create_fulfillment_usecase.handle_request()

    return fulfillments


@router.get(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeFulfillmentRequest"])],
    response_model=List[FulfillmentRequestOut],
)
async def get_fulfillments(get_fulfillments_usecase: GetFulfillmentsUseCase = Depends()):

    fulfillments = await get_fulfillments_usecase.handle_request()

    return fulfillments


@router.put(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["proposeFulfillment"])],
)
async def propose_fulfillment(propose_fulfillments_usecase: ProposeFulfillmentUseCase = Depends()):

    """
    This endpoint is for pharmacies to show their intent on fulfilling an order.
    When they show intent, it then goes to Sales team which makes decision on confirming
    a proposal through order confirmation.
    """

    propose_fulfillments_usecase.handle_request()

    return {"message": "ok"}


@router.put(
    "/packing/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["readyForPickup"])],
)
async def set_ready_for_pickup(mark_ready_for_pickup_usecase: FulfillmentReadyForPickupUseCase = Depends()):

    """
    This endpoint is for pharmacies to tell HealthPe that the package is ready for pickup - they are
    done with packing.
    When they show intent, it then goes to HealthPe team which works with shipping partner to ship
    the medicines to customer.
    """

    await mark_ready_for_pickup_usecase.handle_request()

    return {"message": "ok"}
