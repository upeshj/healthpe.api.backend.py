from fastapi import APIRouter, Depends, Security, status
from .....core.deps import get_database
from ...auth.deps import authorize
from ..usecases.usecase_create_order import CreateOrderUseCase
from ..usecases.usecase_cancel_order import CancelOrderUseCase
from ..usecases.usecase_allocate_order import AllocateOrderUseCase
from ..usecases.usecase_get_order import GetOrderUseCase
from ..usecases.usecase_confirm_order import ConfirmOrderUseCase
from ..usecases.usecase_picked_order import PickedOrderUseCase
from ..usecases.usecase_deliver_order import DeliverOrderUseCase

# from ..usecases.usecase_initiate_fulfillment_request import InitiateFulfillmentRequestUseCase
from ..schemas.order_schema import OrderInDB, OrderOut
from .fulfillments import router as fulfillments_router

router = APIRouter()
router.include_router(fulfillments_router, prefix="/fulfillment", tags=["Fulfillment"])

ORDERING_ROOT = "/"
ORDERING_ORDER_ID = "/{order_id}"
ORDERING_CANCELLATION = ORDERING_ORDER_ID + "/cancellation/"
ORDERING_ALLOCATION = ORDERING_ORDER_ID + "/allocation/"
ORDERING_CONFIRMATION = ORDERING_ORDER_ID + "/confirmation/"
ORDERING_PICKED = ORDERING_ORDER_ID + "/pickedup/"
ORDERING_DELIVERY = ORDERING_ORDER_ID + "/delivery/"


@router.post(
    ORDERING_ROOT,
    dependencies=[Depends(get_database), Security(authorize, scopes=["createOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_201_CREATED,
)
async def create_order(create_order_usecase: CreateOrderUseCase = Depends()):
    """
    Endpoint used to create order
    """

    new_order = await create_order_usecase.handle_request()

    return new_order


@router.get(
    ORDERING_ORDER_ID,
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def get_order_by_id(get_order_usercase: GetOrderUseCase = Depends()):

    return await get_order_usercase.handle_request()


@router.put(
    ORDERING_CANCELLATION,
    dependencies=[Depends(get_database), Security(authorize, scopes=["cancelOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def cancel_order(cancel_order_usecase: CancelOrderUseCase = Depends()):
    """
    Endpoint to cancel an order
    """

    return await cancel_order_usecase.handle_request()


@router.put(
    ORDERING_ALLOCATION,
    dependencies=[Depends(get_database), Security(authorize, scopes=["allocateOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def allocate_order(allocate_order_usecase: AllocateOrderUseCase = Depends()):
    """
    Endpoint to allocate an order
    """

    return await allocate_order_usecase.handle_request()


@router.put(
    ORDERING_CONFIRMATION,
    dependencies=[Depends(get_database), Security(authorize, scopes=["confirmOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def confirm_order(confirm_order_usecase: ConfirmOrderUseCase = Depends()):
    """
    Endpoint to confirm an order
    """

    return await confirm_order_usecase.handle_request()


@router.put(
    ORDERING_PICKED,
    dependencies=[Depends(get_database), Security(authorize, scopes=["pickedOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def picked_order(picked_order_usecase: PickedOrderUseCase = Depends()):
    """
    Endpoint to mark an order in-transit/picked
    """

    return await picked_order_usecase.handle_request()


@router.put(
    ORDERING_DELIVERY,
    dependencies=[Depends(get_database), Security(authorize, scopes=["deliverOrder"])],
    response_model=OrderOut,
    status_code=status.HTTP_200_OK,
)
async def delivered_order(deliver_order_usecase: DeliverOrderUseCase = Depends()):
    """
    Endpoint to mark an order delivered
    """

    return await deliver_order_usecase.handle_request()
