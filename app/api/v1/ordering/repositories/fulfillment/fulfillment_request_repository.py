import logging
import copy
from datetime import datetime
from typing import List
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound

from app.config import settings
from app.core.base_repository import BaseRepository

from ....auth.models.user_model import User
from ...models.order_model import Order
from ...schemas.fulfillment.fulfillment_request_schema import FulfillmentRequestInDB
from ...schemas.fulfillment.fulfillment_proposal_schema import FulfillmentProposalInDB
from ...schemas.fulfillment.fulfillment_proposal_schema import ConfirmFulfillmentProposal
from ...schemas.fulfillment.fulfillment_packed_schema import PackedFulfillmentSchema
from ...models.fulfillment.fulfillment_request_model import FulfillmentRequest
from ...models.fulfillment.fulfillment_item_model import FulfillmentItem
from ...models.fulfillment.fulfillment_request_status_enum import FulfillmentRequestStatus

logger = logging.getLogger(__name__)


class FulfillmentRequestRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_fulfillment(self, fulfillment: FulfillmentRequestInDB) -> FulfillmentRequest:

        fulfillment_copy = copy.deepcopy(fulfillment.dict())

        fulfillment_item_list = []

        for fulfillment_item in fulfillment.items:
            fulfillment_item_list.append(FulfillmentItem(**fulfillment_item.dict()))

        fulfillment_copy.pop("items")

        new_fulfillment = FulfillmentRequest(**fulfillment_copy, items=fulfillment_item_list)

        self.session.add(new_fulfillment)

        await self.session.flush()

        return new_fulfillment

    async def get_fulfillments_delivery_team_associate(self, associate_id: int) -> List[FulfillmentRequest]:

        """
        This method is to be used for healthpe team associate.
        """

        stmt = (
            select(FulfillmentRequest)
            .join(FulfillmentRequest.order)
            .join(Order.current_associate_details)
            .where(Order.current_associate == associate_id)
        )

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        result = await self.session.execute(stmt)

        fulfillments = []
        for (fulfillment,) in result.unique().fetchall():
            fulfillments.append(fulfillment)

        return fulfillments

    async def get_fulfillments_delivery_team_manager(self) -> List[FulfillmentRequest]:

        """
        This method is to be used for healthpe team manager.
        """

        stmt = select(FulfillmentRequest)

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        result = await self.session.execute(stmt)

        fulfillments = []
        for (fulfillment,) in result.unique().fetchall():
            fulfillments.append(fulfillment)

        return fulfillments

    async def get_fulfillments_fulfillment_team(self, location_id: int) -> List[FulfillmentRequest]:

        """
        This method is to be used for healthpe team associate.
        """

        stmt = select(FulfillmentRequest).where(FulfillmentRequest.partner_location_id == location_id)

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        result = await self.session.execute(stmt)

        fulfillments = []
        for (fulfillment,) in result.unique().fetchall():
            fulfillments.append(fulfillment)

        return fulfillments

    async def set_fulfillment_proposal(self, proposal: FulfillmentProposalInDB):

        update_time = datetime.now()

        for item in proposal.items:
            stmt = (
                update(FulfillmentItem)
                .where(FulfillmentItem.fulfillment_id == proposal.id, FulfillmentItem.id == item.id)
                .values(
                    fulfillable_quantity=item.fulfillable_quantity,
                    quoted_price=item.quoted_price,
                    updated_by=proposal.proposed_by,
                    updated_at=update_time,
                )
            )

            await self.session.execute(stmt)

        stmt = (
            update(FulfillmentRequest)
            .where(
                FulfillmentRequest.id == proposal.id, FulfillmentRequest.partner_location_id == proposal.location_id
            )
            .values(status=proposal.status, updated_at=update_time, proposed_by=proposal.proposed_by)
        )

        logger.debug(f"Setting proposal: {proposal}")

        await self.session.execute(stmt)

        await self.session.flush()

        return

    async def confirm_fulfillment_proposal(self, proposal: ConfirmFulfillmentProposal):

        stmt = (
            update(FulfillmentRequest)
            .where(FulfillmentRequest.id == proposal.id)
            .values(status=FulfillmentRequestStatus.Confirmed, updated_at=datetime.now())
        )

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        await self.session.execute(stmt)
        await self.session.flush()

    async def update_invoice_file_and_status(self, fulfillment: PackedFulfillmentSchema):

        stmt = (
            update(FulfillmentRequest)
            .where(FulfillmentRequest.id == fulfillment.id)
            .values(status=fulfillment.status, invoice_file=fulfillment.invoice_file, updated_at=datetime.now())
        )

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        await self.session.execute(stmt)
        await self.session.flush()
