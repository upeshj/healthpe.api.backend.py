import logging
from datetime import datetime
from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError

from .....config import settings
from .....core.base_repository import BaseRepository
from ..models.order_model import OrderStatus
from ..models.order_history_model import OrderHistory
from ..schemas.order_history_schema import OrderHistorySchema

logger = logging.getLogger(__name__)


class OrderHistoryRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_order_history(self, record: OrderHistorySchema) -> None:

        # order_history = OrderHistory(
        #     current_associate=6,
        #     previous_associate=6,
        #     current_status=OrderStatus.CANCELLED,
        #     previous_status=OrderStatus.PENDING,
        # )

        order_history = OrderHistory(**record.dict())

        self.session.add(order_history)
        await self.session.flush()
