import logging
from datetime import datetime
from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError

from .....config import settings
from .....core.base_repository import BaseRepository
from app.core.errors import DBError
from ..models.order_model import OrderStatus
from ..models.order_model import Order
from ..models.order_item_model import OrderItem
from ..schemas.order_schema import OrderInDB
from ..schemas.order_item_schema import OrderItemInDB

logger = logging.getLogger(__name__)


class OrderRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_order(self, order: OrderInDB, order_items: List[OrderItemInDB]) -> Order:

        order_item_list = []

        for order_item in order_items:
            order_item_list.append(OrderItem(**order_item.dict()))

        new_order = Order(**order.dict(), items=order_item_list)

        try:
            self.session.add(new_order)
            await self.session.flush()
            await self.session.refresh(new_order)
            return new_order
        except Exception as e:
            logger.exception(f"{str(e)}", exc_info=True)

            raise DBError(code="DB", message=str(e))

    async def get_order_by_id(self, order_id: int) -> Order:

        stmt = select(Order).where(Order.id == order_id)

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        try:
            response = await self.session.execute(stmt)
            (order,) = response.unique().one()

            return order
        except NoResultFound as nrf:
            logger.error(f"Order id {order_id} not found")
            return None
        except Exception as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise DBError(code="DB", message=str(e))

    async def cancel_order(self, order: Order) -> Order:

        stmt = (
            update(Order).where(Order.id == order.id).values(status=OrderStatus.CANCELLED, updated_at=datetime.now())
        )
        try:
            await self.session.execute(stmt)
            await self.session.refresh(order)
        except Exception as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise DBError(code="DB", message=str(e))

        return order

    async def update_status(self, order: Order, new_status: str) -> Order:

        stmt = update(Order).where(Order.id == order.id).values(status=new_status, updated_at=datetime.now())

        try:
            await self.session.execute(stmt)
            await self.session.refresh(order)
        except Exception as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise DBError(code="DB", message=str(e))

        return order

    async def update_associate(self, order: Order, associate: int) -> Order:

        stmt = update(Order).where(Order.id == order.id).values(current_associate=associate, updated_at=datetime.now())

        try:
            await self.session.execute(stmt)
            await self.session.refresh(order)
        except Exception as e:
            logger.exception(f"{str(e)}", exc_info=True)
            raise DBError(code="DB", message=str(e))

        return order
