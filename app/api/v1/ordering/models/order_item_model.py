from enum import Enum
from datetime import datetime
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from app.core.base import Base
from app.core.base_model import HealthPeModelBase


class OrderItemStatus(str, Enum):

    PENDING = "Pending"
    OPEN = "Open"
    ACCEPTED = "Accepted"
    CONFIRMED = "Confirmed"
    READY_FOR_PICKUP = "Ready for pickup"
    IN_TRANSIT = "In transit"
    DELIVERED = "Delivered"


class OrderItem(HealthPeModelBase):

    __tablename__ = "order_items"

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    quantity = Column(Integer, nullable=False)
    order_id = Column(Integer, ForeignKey("orders.id"), nullable=False)
    status = Column(sa.Enum(OrderItemStatus), nullable=False)

    order = relationship("Order", back_populates="items")
    fulfillment = relationship("OrderItemFulfillment", back_populates="order_item")
    # fulfilling_pharmacy = Column(Integer, ForeignKey("partners.id"))
