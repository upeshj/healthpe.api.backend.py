from app.core.base_model import HealthPeModelBase
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DECIMAL
from .fulfillment_request_status_enum import FulfillmentRequestStatus


class FulfillmentRequest(HealthPeModelBase):

    __tablename__ = "fulfillment_requests"

    id = Column(Integer, primary_key=True)
    created_by = Column(Integer, ForeignKey("users.id"))
    proposed_by = Column(Integer, ForeignKey("users.id"), nullable=True)
    order_id = Column(Integer, ForeignKey("orders.id"))
    partner_location_id = Column(Integer, ForeignKey("partner_addresses.id"))
    status = Column(sa.Enum(FulfillmentRequestStatus), default=FulfillmentRequestStatus.Open)
    expiration_time = Column(DateTime, nullable=False)
    invoice_file = Column(String(100), nullable=True)

    items = relationship("FulfillmentItem", lazy="joined", innerjoin=False)
    order = relationship("Order", back_populates="fulfillment_requests")
