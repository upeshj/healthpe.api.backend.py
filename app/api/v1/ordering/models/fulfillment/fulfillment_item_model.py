from app.core.base_model import HealthPeModelBase
from datetime import datetime
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DECIMAL
from .fulfillment_request_status_enum import FulfillmentRequestStatus


class FulfillmentItem(HealthPeModelBase):

    __tablename__ = "fulfillment_items"

    id = Column(Integer, primary_key=True)
    updated_by = Column(Integer, ForeignKey("users.id"))
    fulfillment_id = Column(Integer, ForeignKey("fulfillment_requests.id"), nullable=False)
    order_item_id = Column(Integer, ForeignKey("order_items.id"), nullable=False)
    name = Column(String(255), nullable=False)
    requested_quantity = Column(Integer, nullable=False)
    fulfillable_quantity = Column(Integer, nullable=True)
    quoted_price = Column(DECIMAL, nullable=True)
