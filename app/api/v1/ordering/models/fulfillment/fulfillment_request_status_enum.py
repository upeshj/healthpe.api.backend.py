from enum import Enum


class FulfillmentRequestStatus(str, Enum):

    Open = "Open"
    Proposed = "Proposed"
    Confirmed = "Confirmed"
    ReadyForPickup = "ReadyForPickup"
