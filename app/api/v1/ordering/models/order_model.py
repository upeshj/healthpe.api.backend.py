from datetime import datetime
from sqlalchemy.orm import declared_attr
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from app.core.base import Base
from app.core.base_model import HealthPeModelBase
from enum import Enum


class OrderStatus(str, Enum):

    PENDING = "Pending"
    OPEN = "Open"
    CONFIRMED = "Confirmed"
    PAYMENT_PENDING = "Payment pending"
    PAYMENT_DONE = "Paid"
    READY_FOR_PICKUP = "Ready for pickup"
    IN_TRANSIT = "In transit"
    DELIVERED = "Delivered"
    CANCELLED = "Cancelled"


class Order(HealthPeModelBase):

    __tablename__ = "orders"

    id = Column(Integer, primary_key=True)
    creator = Column(Integer, ForeignKey("users.id"), nullable=False)
    customer = Column(Integer, ForeignKey("customers.id"), nullable=False)
    delivery_address_id = Column(Integer, ForeignKey("customer_addresses.id"), nullable=False)
    current_associate = Column(Integer, ForeignKey("users.id"), nullable=True)
    status = Column(sa.Enum(OrderStatus), nullable=False, default=OrderStatus.PENDING)

    items = relationship("OrderItem", back_populates="order", uselist=True, lazy="joined", innerjoin=True)
    fulfillments = relationship("OrderItemFulfillment", back_populates="order")

    creator_details = relationship("User", foreign_keys=[creator], lazy="joined", innerjoin=True)
    customer_details = relationship("Customer", foreign_keys=[customer], lazy="joined", innerjoin=True)
    delivery_address = relationship("CustomerAddress", foreign_keys=[delivery_address_id], lazy="joined")
    fulfillment_requests = relationship("FulfillmentRequest", lazy="selectin", innerjoin=False, back_populates="order")
    current_associate_details = relationship("User", foreign_keys=[current_associate], lazy="joined")
