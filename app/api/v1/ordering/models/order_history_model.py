from datetime import datetime
from sqlalchemy.orm import declared_attr
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from app.core.base import Base
from app.core.base_model import HealthPeModelBase
from enum import Enum


class OrderHistory(HealthPeModelBase):

    __tablename__ = "order_history"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("orders.id"), nullable=False)
    current_associate = Column(Integer, ForeignKey("users.id"), nullable=True)
    previous_associate = Column(Integer, ForeignKey("users.id"), nullable=True)
    current_status = Column(String(20), nullable=False)
    previous_status = Column(String(20), nullable=False)
