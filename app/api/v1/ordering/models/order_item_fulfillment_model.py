from datetime import datetime
import sqlalchemy as sa
from sqlalchemy import Column, Integer, DECIMAL, String
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from app.core.base import Base
from app.core.base_model import HealthPeModelBase


class OrderItemFulfillment(HealthPeModelBase):

    __tablename__ = "order_item_fulfillments"

    id = Column(Integer, primary_key=True)
    fulfillable_quantity = Column(Integer, nullable=False)
    price = Column(DECIMAL, nullable=False)
    status = Column(String, nullable=False)

    partner_code = Column(Integer, ForeignKey("partners.code"), nullable=False)
    associate_id = Column(Integer, ForeignKey("users.id"), nullable=False)
    order_item_id = Column(Integer, ForeignKey("order_items.id"), nullable=False)
    order_id = Column(Integer, ForeignKey("orders.id"), nullable=False)
    shipping_id = Column(Integer, ForeignKey("shipments.id"), nullable=True)

    order = relationship("Order", back_populates="fulfillments")
    order_item = relationship("OrderItem", back_populates="fulfillment")
    shipping = relationship("Shipment", back_populates="fulfillments")
