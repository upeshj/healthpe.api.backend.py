from typing import Optional, Union
from enum import Enum
from pydantic import BaseModel, constr, EmailStr, Field
from app.core.partners_and_roles import RolesEnum
from datetime import datetime


class BaseUser(BaseModel):

    first_name: constr(max_length=30)
    last_name: Optional[constr(max_length=30)] = None
    username: constr(max_length=30)
    email: EmailStr
    partner_code: int
    location_id: int
    role: RolesEnum
    phone_number: constr(max_length=15)
    poc: bool
    manager_id: Union[int, None]
    admin_id: Union[int, None]

    class Config:
        orm_mode = True


class CreateUser(BaseUser):

    password: str  # TODO: Need validator to adhere to a pattern


class LoginUser(BaseModel):

    username: str
    password: str
    # partner_code: int


class UserInDB(BaseUser):

    hashed_password: str
    last_password_change_time: Optional[datetime] = Field(default_factory=datetime.now)
    id_proof_object_name: str


class UserOut(BaseUser):

    pass
