from pydantic import BaseModel


class VerifyOTPSchema(BaseModel):

    username: str
    otp: str
