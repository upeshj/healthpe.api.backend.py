from pydantic import BaseModel


class ForgotPasswordSchema(BaseModel):

    # username: str
    partner_code: int
    identity: str


class OTPSentSchema(BaseModel):

    phone_number: str
    email: str

    class Config:
        orm_mode = True
