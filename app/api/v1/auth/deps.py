import logging
from devtools import debug
from app.core.security import oauth2_scheme
from fastapi import Depends, Request, HTTPException, status
from fastapi.security import SecurityScopes
from app.core.security import get_token_data, TokenData
from .repositories.user_repository import UserRepository
from app.core.deps import current_user
from .models.user_model import User
from .permissions import permission_matrix
from app.core.partners_and_roles import PartnerType

logger = logging.getLogger(__name__)


async def get_current_user(
    req: Request, token_data: TokenData = Depends(get_token_data), user_repo: UserRepository = Depends()
):

    user = await user_repo.get_user_by_username(token_data.username)

    if not user:
        raise HTTPException(
            status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )

    # Make the user available in the request so it can accessed everywhere
    req.state.current_user = user

    # Make it also available in context vars so that it is available in places where request is not accessible
    # It can be accessed from anywhere using:
    #   from app.core.deps import current_user
    #   current_user.get()
    current_user.set(user)

    return user


async def authorize(
    security_scopes: SecurityScopes,
    user: User = Depends(get_current_user),
) -> None:

    user_partner_type = user.partner.ptype

    partner_roles = permission_matrix.get(user_partner_type, None)

    if not partner_roles:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="You are not authorized to perform this action")

    role_permissions = partner_roles.get(user.role, None)

    if not role_permissions:
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="You are not authorized to perform this action")

    # TODO: Take this hardcoding out. See how to centralize PartnerType Schema
    if "everything" in role_permissions:

        logger.info("Admin is performing this action")
        return

    for scope in security_scopes.scopes:

        if scope not in role_permissions:
            raise HTTPException(status.HTTP_401_UNAUTHORIZED, detail="You are not authorized to perform this action")

    logger.warn(f"User ID {user.id}:{user.username}:{user_partner_type} authorized for {security_scopes.scope_str}")
    # Check the user partner, role and match permission.
    # If not, throw HTTP_401_UNAUTHORIZED

    # else continue
    return
