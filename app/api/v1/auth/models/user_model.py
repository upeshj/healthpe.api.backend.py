# from typing import Boolean
from datetime import datetime
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils import EmailType
from sqlalchemy_utils.types import PhoneNumber
from app.core.base import Base
from app.core.base_model import HealthPeModelBase


class User(HealthPeModelBase):

    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    first_name = Column(String(50), nullable=False)
    last_name = Column(String(50), nullable=False)
    username = Column(String(12), nullable=False)
    hashed_password = Column(String(200), nullable=False)
    phone_number = Column(String(15), nullable=False)
    email = Column(EmailType, nullable=True)
    is_active = Column(Boolean, nullable=False, default=True)
    # country_code = Column(Unicode(8), nullable=False)
    partner_code = Column(Integer, ForeignKey("partners.code"), nullable=False)
    location_id = Column(Integer, ForeignKey("partner_addresses.id"), nullable=False)
    otp = Column(String(6), nullable=True, default="")
    last_login = Column(DateTime, nullable=True)
    last_password_change_time = Column(DateTime, nullable=False, default=datetime.now)
    role = Column(String(10), nullable=False)
    poc = Column(Boolean, nullable=False)
    id_proof_object_name = Column(String(50), nullable=True)
    manager_id = Column(Integer, ForeignKey("users.id"), nullable=True)
    admin_id = Column(Integer, ForeignKey("users.id"), nullable=True)

    # _phone_number = sa.orm.composite(PhoneNumber, phone_number, country_code=country_code)
    partner = relationship("Partner", lazy="joined", innerjoin=True)
