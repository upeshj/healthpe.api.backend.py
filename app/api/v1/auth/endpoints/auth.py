from devtools import debug
from fastapi import APIRouter, Depends, status, Request, Security
from ..deps import get_current_user, authorize
from .....core.deps import get_database
from ..schemas.user_schema import UserOut
from ..schemas.forgot_password_schema import OTPSentSchema
from ..usecases.usecase_register_user import RegisterUserUseCase
from ..usecases.usecase_login_user import LoginUserUseCase
from ..usecases.usecase_forgot_password import ForgotPasswordUseCase
from ..usecases.usecase_forgot_password_verify_otp import ForgotPasswordVerifyOTPUseCase
from ..usecases.usecase_change_password import ChangePasswordUseCase
from app.core.security import must_be_logged_in

router = APIRouter()


@router.post(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["addUser"])],
    response_model=UserOut,
    status_code=status.HTTP_201_CREATED,
)
async def register(register_user_usecase: RegisterUserUseCase = Depends()):

    new_user = await register_user_usecase.handle_request()

    return new_user


@router.post("/login", dependencies=[Depends(get_database)], status_code=status.HTTP_200_OK)
async def login(login_user_usecase: LoginUserUseCase = Depends()):

    result = await login_user_usecase.handle_request()

    return result


@router.put(
    "/user/{user_id}/password/change",
    dependencies=[Depends(get_database), Depends(must_be_logged_in)],
)
async def change_password(change_password_usecase: ChangePasswordUseCase = Depends()):

    await change_password_usecase.handle_request()

    return {}


@router.post(
    "/user/{user_id}/password/forgot",
    dependencies=[Depends(get_database)],
    response_model=OTPSentSchema,
    status_code=status.HTTP_200_OK,
)
async def forgot(forgot_password_usecase: ForgotPasswordUseCase = Depends()):

    return await forgot_password_usecase.handle_request()


@router.put("/user/{user_id}/password/verifyotp", dependencies=[Depends(get_database)])
async def verifyotp(verify_otp_usecase: ForgotPasswordVerifyOTPUseCase = Depends()):

    await verify_otp_usecase.handle_request()

    return {}
