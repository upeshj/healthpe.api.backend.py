import logging
from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from ..repositories.user_repository import UserRepository
from ..schemas.change_password_schema import ChangePasswordSchema
from app.core.security import verify_password, hash_password

logger = logging.getLogger(__name__)


class ChangePasswordUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        change_password_details: ChangePasswordSchema,
        user_repository: UserRepository = Depends(),
    ):

        self.req = req
        self.res = res
        self.change_password_details = change_password_details
        self.user_repository = user_repository

    async def execute(self):

        existing_user = await self.user_repository.get_user_by_username_and_partner_code(
            self.change_password_details.username, self.change_password_details.partner_code
        )

        if not existing_user:
            logger.info(
                f"User {self.change_password_details.username} for partner {self.change_password_details.partner_code} does not exist"
            )
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User does not exist")

        if not verify_password(self.change_password_details.current_password, existing_user.hashed_password):
            raise HTTPException(status.HTTP_406_NOT_ACCEPTABLE, detail="Current password does not match")

        hashed_password = hash_password(self.change_password_details.new_password)

        await self.user_repository.set_password(existing_user, hashed_password)

        return None
