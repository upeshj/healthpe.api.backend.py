import logging
import tempfile
import os
from typing import Optional, Union
from pydantic import EmailStr
from fastapi import Request, Response, Depends, File, UploadFile, Form
from fastapi import HTTPException, status
from app.core.partners_and_roles import RolesEnum
from app.core.base_usecase import BaseUseCase
from app.core.security import hash_password
from app.core.integrations.storage.errors import StorageServiceError
from ..repositories.user_repository import UserRepository
from ..schemas.user_schema import CreateUser, UserInDB, UserOut
from ...partner.repositories.partner_repository import PartnerRepository
from ...integrations.storage.id_proofs_storage import IDProofsStorageService

logger = logging.getLogger(__name__)


class RegisterUserUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        # user: CreateUser,
        first_name: str = Form(...),
        last_name: Optional[str] = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        email: EmailStr = Form(...),
        partner_code: int = Form(...),
        location_id: int = Form(...),
        role: RolesEnum = Form(...),
        phone_number: str = Form(...),
        poc: bool = Form(...),
        manager_id: Union[int, None] = Form(None),
        admin_id: Union[int, None] = Form(None),
        id_proof_image: UploadFile = File(...),
        user_repository: UserRepository = Depends(),
        partner_repository: PartnerRepository = Depends(),
        id_proofs_storage: IDProofsStorageService = Depends(),
    ) -> None:

        super().__init__(req, res)
        self.user = CreateUser(
            first_name=first_name,
            last_name=last_name,
            username=username,
            password=password,
            email=email,
            partner_code=partner_code,
            location_id=location_id,
            role=role,
            phone_number=phone_number,
            poc=poc,
            manager_id=manager_id,
            admin_id=admin_id,
        )

        self.id_proof_image = id_proof_image
        self.id_proof_storage = id_proofs_storage
        self.user_repository = user_repository
        self.partner_repository = partner_repository

    async def execute(self):

        await self.check_if_partner_exists()
        await self.validate_existing_user()

        # Save the file locally with some unique name
        id_proof_file_name = await self._save_id_proof()

        hashed_password = hash_password(self.user.password)

        db_user = UserInDB(
            **self.user.dict(), hashed_password=hashed_password, id_proof_object_name=id_proof_file_name
        )

        new_user = await self.user_repository.create_user(db_user)

        return new_user

    async def check_if_partner_exists(self):

        # TODO: Need to think about this.
        # if self.user.partner_code != self.req.state.current_user.partner_code:
        #     raise HTTPException(status.HTTP_401_UNAUTHORIZED, "You can add users to your own organization.")

        partner = await self.partner_repository.get_partner_by_code(self.user.partner_code)

        if not partner:
            raise HTTPException(status.HTTP_404_NOT_FOUND, f"Partner {self.user.partner_code} not found")

        address_exists = filter(lambda address: address.id == self.user.location_id, partner.addresses)

        if not any(address_exists):
            raise HTTPException(status.HTTP_400_BAD_REQUEST, f"Location ID {self.user.location_id} does not exist")

    async def validate_existing_user(self):

        """
        We are considering username, phone_number and email unique across the database and not unique by partner.
        This is because let's say, the admin team creates same user name for two people
        under different partners. While logging in, the end user somehow got hold of another
        partner_id, they will be able to login using that partner_id.
        """

        # existing_user = await self.user_repository.get_user_by_username_or_email_or_phone(
        #     self.user.username, self.user.email, self.user.phone_number
        # )
        existing_user = await self.user_repository.get_user_by_phone_and_partner_code(
            self.user.phone_number, self.user.partner_code
        )

        if existing_user:
            raise HTTPException(
                status.HTTP_410_GONE,
                f"Username {self.user.username} already exists.",
            )

    async def _save_id_proof(self) -> str:
        """
        This method will save the id proof to local temp, then upload to cloud storage and then cleanup the
        local file. We have not optimized the S3 upload since the volume will be very little for this activity.
        Once we see the need to scale, we can upload the file to S3 as an offline task so that the response
        time to users is not compromised.

        Raises:
            HTTPException: [description]

        Returns:
            str: The filename without directory. This filename will be the same that can be seen on cloud storage in
            the respective bucket. The object and bucket are private but to access, use the get_presigned_link
            method on StorageService.
        """

        id_proof_file_name = (
            f"{self.user.partner_code}_{self.user.username}_{self.user.phone_number}_{self.id_proof_image.filename}"
        )

        logger.info(
            f"File {self.id_proof_image.filename} having length {self.id_proof_image} will be saved as {id_proof_file_name}"
        )

        full_file_path = os.path.join(tempfile.gettempdir(), id_proof_file_name)
        with open(full_file_path, "wb") as output_file:
            output_file.write(await self.id_proof_image.read())

        try:
            await self.id_proof_storage.save_file(full_file_path, self.id_proof_image.content_type)

            # Cleanup the local storage
            os.remove(full_file_path)

        except StorageServiceError as e:
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR,
                "Error occured while trying to upload ID Proof. Please try again.",
            )

        return id_proof_file_name
