import logging
import random
from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from ..repositories.user_repository import UserRepository
from ...partner.repositories.partner_repository import PartnerRepository
from ..schemas.user_schema import LoginUser
from ..schemas.forgot_password_schema import ForgotPasswordSchema
from ..models.user_model import User
from app.core.integrations.otp.otp_service import OTPService
from app.core.integrations.otp.sms_otp_type import SMSOtp
from app.core.integrations.otp.errors import OTPServiceError

logger = logging.getLogger(__name__)


class ForgotPasswordUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        forgot_password_details: ForgotPasswordSchema,
        user_repository: UserRepository = Depends(),
        partner_repository: PartnerRepository = Depends(),
        otp_service: OTPService = Depends(),
    ):

        self.req = req
        self.res = res
        self.forgot_password_details = forgot_password_details
        self.user_repository = user_repository
        self.partner_repository = partner_repository
        self.otp_service = otp_service
        random.seed(1234)

    async def execute(self):

        existing_user = await self.user_repository.get_user_by_username_or_phone_and_partner_code(
            self.forgot_password_details.identity,
            self.forgot_password_details.identity,
            self.forgot_password_details.partner_code,
        )

        # existing_user = await self.user_repository.get_user_by_username_and_partner_code(
        #     self.forgot_password_details.username, self.forgot_password_details.partner_code
        # )

        if not existing_user:
            logger.info(f"User {self.forgot_password_details.identity} does not exist")
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User does not exist")

        otp = int(random.random() * 1000000)

        await self.user_repository.set_otp(existing_user, str(otp))

        try:
            await self.otp_service.send_otp(SMSOtp(existing_user.phone_number, str(otp)))

            # TODO: Send OTP through email as well
            return existing_user
        except OTPServiceError as ose:
            logger.error(f"{str(ose)}")
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR, "Internal Server Error occured. Please try later"
            )
