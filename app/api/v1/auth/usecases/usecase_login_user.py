from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from ..repositories.user_repository import UserRepository
from ...partner.repositories.partner_repository import PartnerRepository
from ..schemas.user_schema import LoginUser
from .....core.security import verify_password, create_jwt_token
from ..models.user_model import User


class LoginUserUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        user: LoginUser,
        user_repository: UserRepository = Depends(),
        partner_repository: PartnerRepository = Depends(),
    ):

        self.req = req
        self.res = res
        self.user = user
        self.user_repository = user_repository
        self.partner_repository = partner_repository

    async def execute(self):

        user_from_db = await self.get_user()

        # Verify password
        if not verify_password(self.user.password, user_from_db.hashed_password):
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED, f"Incorrect username/password", headers={"WWW-Authenticate": "Bearer"}
            )

        # Generate JWT
        token = create_jwt_token({"sub": user_from_db.username})
        self.res.headers["Access-Token"] = token

        # Update last_login
        await self.user_repository.update_last_login(user_from_db)

        return {"access_token": token, "token_type": "bearer"}

    async def get_user(self) -> User:

        # existing_user = await self.user_repository.get_user_by_username_and_partner_code(
        #     self.user.username, self.user.partner_code
        # )
        existing_user = await self.user_repository.get_user_by_username(self.user.username)

        if not existing_user:
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED,
                f"User {self.user.username} does not exist",
                headers={"WWW-Authenticate": "Bearer"},
            )

        # TODO: Check if user is banned

        partner = existing_user.partner
        # partner = await self.partner_repository.get_partner_by_code(self.user.partner_code)
        # if not partner:
        #     raise HTTPException(
        #         status.HTTP_401_UNAUTHORIZED,
        #         f"Wrong partner code {self.user.partner_code}",
        #         headers={"WWW-Authenticate": "Bearer"},
        #     )

        if partner.is_banned:
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED,
                f"Partner {self.user.partner_code} is banned. Please contact your administrator.",
                headers={"WWW-Authenticate": "Bearer"},
            )

        return existing_user
