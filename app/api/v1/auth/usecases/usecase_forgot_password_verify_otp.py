import logging
import random
from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from ..repositories.user_repository import UserRepository
from ...partner.repositories.partner_repository import PartnerRepository
from ..schemas.user_schema import LoginUser
from ..schemas.verify_otp_schema import VerifyOTPSchema
from ..models.user_model import User
from app.core.integrations.otp.otp_service import OTPService
from app.core.integrations.otp.sms_otp_type import SMSOtp

logger = logging.getLogger(__name__)


class ForgotPasswordVerifyOTPUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        otp_verification_details: VerifyOTPSchema,
        user_repository: UserRepository = Depends(),
    ):

        self.req = req
        self.res = res
        self.user_repository = user_repository
        self.otp_verification_details = otp_verification_details

    async def execute(self):

        existing_user = await self.user_repository.get_user_by_username(self.otp_verification_details.username)

        if not existing_user:
            raise HTTPException(status.HTTP_404_NOT_FOUND, detail="User not found")

        if not existing_user.otp or existing_user.otp != self.otp_verification_details.otp:
            raise HTTPException(status.HTTP_406_NOT_ACCEPTABLE, detail="Incorrect OTP")

        logger.info(f"Resetting the OTP for user {existing_user.id}")
        await self.user_repository.set_otp(existing_user, None)

        return None
