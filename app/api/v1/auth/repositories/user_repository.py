import logging
from typing import List
from devtools import debug
from datetime import datetime
from sqlalchemy import select, insert, or_, update, and_
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import joinedload

from ..schemas.user_schema import CreateUser, UserInDB
from ..models.user_model import User
from .....core.base_repository import BaseRepository

logger = logging.getLogger(__name__)


class UserRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    # async def get_all_partners(self) -> List[Partner]:

    #     result = await self.session.execute(select(Partner))

    #     partners = result.scalars().all()

    #     return partners

    # async def get_partner_by_code(self, code: str) -> Partner:

    #     stmt = select(Partner).where(Partner.code == code)

    #     result = await self.session.execute(stmt)

    #     # TODO: Need to move this exception handling to usecase
    #     try:
    #         (partner,) = result.one()
    #     except NoResultFound:
    #         return None

    #     return partner

    async def create_user(self, user: UserInDB) -> User:

        db_user = User(**user.dict())

        self.session.add(db_user)
        await self.session.flush()
        # TODO: Raise exception

        return db_user

    async def get_user_by_phone_and_partner_code(self, phone: str, partner_code: int) -> User:

        result = await self.session.execute(
            select(User).where(User.phone_number == phone, User.partner_code == partner_code)
        )

        try:
            (existing_user,) = result.one()
        except NoResultFound:
            return None

        return existing_user

    async def get_user_by_username_and_partner_code(self, username: str, partner_code: int) -> User:

        result = await self.session.execute(
            select(User).where(User.username == username, User.partner_code == partner_code)
        )

        try:
            (existing_user,) = result.one()
        except NoResultFound:
            return None

        return existing_user

    async def get_user_by_username_and_partner_id(self, username: str, partner_id: int) -> User:

        result = await self.session.execute(
            select(User).where(User.username == username, User.partner_id == partner_id)
        )

        try:
            (existing_user,) = result.one()
        except NoResultFound:
            return None

        return existing_user

    async def get_user_by_username(self, username: str) -> User:

        result = await self.session.execute(
            select(User).options(joinedload(User.partner)).where(User.username == username)
        )

        try:
            (existing_user,) = result.one()
        except NoResultFound as exec:
            return None

        return existing_user

    # async def get_user_by_email(self, email: str) -> User:

    #     result = await self.session.execute(select(User).where(User.email == email))

    #     try:
    #         (existing_user,) = result.one()
    #     except NoResultFound as exec:
    #         return None

    #     return existing_user

    # async def get_user_by_username_or_email_or_phone(self, username: str, email: str, phone_number: str) -> User:

    #     result = await self.session.execute(
    #         select(User).where(or_(User.email == email, User.username == username, User.phone_number == phone_number))
    #     )

    #     try:
    #         (existing_user,) = result.fetchone()
    #     except NoResultFound as exec:
    #         return None

    #     return existing_user

    async def get_user_by_username_or_phone_and_partner_code(
        self, username: str, phone_number: str, partner_code: int
    ) -> User:

        stmt = select(User).where(
            and_(
                or_(User.username == username, User.phone_number == phone_number),
                User.partner_code == partner_code,
            )
        )

        logger.debug(f"Compiled SQL: {stmt.compile(self.session.get_bind())}")

        result = await self.session.execute(stmt)

        try:
            (existing_user,) = result.one()
        except NoResultFound as exec:
            return None

        return existing_user

    async def update_last_login(self, user: User) -> None:
        """
        Updates the last login of the user to current time

        Args:
            user (User): User from Database
        """

        user.last_login = datetime.now()

        await self.session.flush()

    async def set_otp(self, user: User, otp: str):

        stmt = update(User).where(User.username == user.username, User.id == user.id).values(otp=otp)

        await self.session.execute(stmt)

    async def set_password(self, user: User, new_password: str):

        stmt = (
            update(User).where(User.id == user.id, User.username == user.username).values(hashed_password=new_password)
        )

        await self.session.execute(stmt)
