from app.core.partners_and_roles import PartnerType, RolesEnum

# TODO: Move this to database
permissions = {"Ordering"}

permission_matrix = {
    PartnerType.sales: {
        RolesEnum.manager: ["createOrder", "cancelOrder", "canAcceptPayment"],
        RolesEnum.associate: [
            "createOrder",
            "cancelOrder",
            "seeCustomerData",
            "createNewCustomer",
            "canAcceptPayment",
            "createInvoice",
            "confirmOrder",
        ],
    },
    PartnerType.delivery: {
        RolesEnum.manager: [
            "allocateOrder",
            "cancelOrder",
            "addUser",
            "seeFulfillmentRequest",
            "deliverOrder",
            "pickedOrder",
        ],
        RolesEnum.associate: [
            "allocateOrder",
            "createFulfillmentRequest",
            "seeFulfillmentRequest",
            "deliverOrder",
            "pickedOrder",
        ],
    },
    PartnerType.fulfillment: {
        RolesEnum.manager: [
            "callForHelp",
            "seeFulfillmentRequest",
            "proposeFulfillment",
            "readyForPickup",
        ],
        RolesEnum.associate: ["seeFulfillmentRequest", "proposeFulfillment", "readyForPickup"],
    },
    PartnerType.admin: {RolesEnum.admin: ["everything"]},
}

# TODO: Create permission groups so it's easier to replicate permissions across roles
