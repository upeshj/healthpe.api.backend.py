from app.core.base_model import HealthPeModelBase
from sqlalchemy import Column, Integer, DECIMAL, String
from sqlalchemy.orm import relationship


class Shipment(HealthPeModelBase):

    __tablename__ = "shipments"

    id = Column(Integer, primary_key=True)
    fulfillments = relationship("OrderItemFulfillment", back_populates="shipping")
