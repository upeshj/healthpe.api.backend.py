from typing import Optional
from pydantic import BaseModel, constr, validator
from sqlalchemy_utils import PhoneNumberType, PhoneNumber


class CustomerBase(BaseModel):

    first_name: str
    last_name: Optional[str] = None
    email: Optional[str] = None
    is_active: Optional[bool] = True

    class Config:
        orm_mode = True


class CreateCustomer(CustomerBase):

    phone_number: constr(max_length=13)

    @validator("phone_number")
    def phone_must_be_with_country_code(cls, v: str):

        if not v.startswith("+"):
            raise ValueError("must start with country code")
        return v


class CustomerInDB(CreateCustomer):

    pass


class CustomerOut(CustomerInDB):

    id: int
