from typing import Optional
from pydantic import BaseModel, constr, validator
from sqlalchemy_utils import PhoneNumberType, PhoneNumber
from .....core.utils import joined_and_single_spaced


class CustomerAddressBase(BaseModel):

    atype: str
    line_1: str
    line_2: Optional[str] = None
    city: str
    state: str
    pincode: str
    country: str

    @property
    def full_address(self):
        return joined_and_single_spaced(
            [
                self.line_1,
                self.line_2,
                self.city,
                self.state,
                self.pincode,
                self.country,
            ],
            " ",
        )

    class Config:
        orm_mode = True


class CreateCustomerAddress(CustomerAddressBase):

    pass


class OrderDeliveryAddress(CreateCustomerAddress):

    address_id: Optional[int] = None


class CustomerAddressInDB(CustomerAddressBase):

    customer_id: int
    long_lat: str


class CustomerAddressOut(CustomerAddressBase):

    id: int
