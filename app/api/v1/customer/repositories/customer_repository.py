from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from .....core.base_repository import BaseRepository
from ..models.customer_model import Customer
from ..schemas.customer_schema import CustomerInDB
from sqlalchemy_utils.types import PhoneNumber


class CustomerRepository(BaseRepository):
    def __init__(self):

        super().__init__()

    async def get_customer_by_phone(self, phone_number: str) -> Customer:

        stmt = select(Customer).where(Customer.phone_number == phone_number)

        result = await self.session.execute(stmt)

        try:
            (customer,) = result.one()
            return customer
        except NoResultFound as exc:
            return None

    async def get_customer_by_id(self, id: int) -> Customer:

        stmt = select(Customer).where(Customer.id == id)

        result = await self.session.execute(stmt)

        try:
            (customer,) = result.one()
            return customer
        except NoResultFound as exc:
            return None

    async def create_new_customer(self, customer: CustomerInDB) -> Customer:

        # new_customer_dict = customer.dict()
        # new_customer_dict["phone_number"] = customer.phone_number
        new_customer = Customer(**customer.dict())

        self.session.add(new_customer)
        await self.session.flush()

        await self.session.refresh(new_customer)

        return new_customer
