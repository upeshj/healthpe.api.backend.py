from typing import List
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from .....core.base_repository import BaseRepository
from ..models.customer_address_model import CustomerAddress
from ..schemas.customer_address_schema import CustomerAddressInDB
from sqlalchemy_utils.types import PhoneNumber


class CustomerAddressRepository(BaseRepository):
    def __init__(self):

        super().__init__()

    async def create_address(self, address: CustomerAddressInDB) -> CustomerAddress:

        new_address = CustomerAddress(**address.dict())

        self.session.add(new_address)
        await self.session.flush()

        await self.session.refresh(new_address)

        return new_address

    async def get_customer_address(self, customer_id: int, address_id: int) -> CustomerAddress:

        stmt = select(CustomerAddress).where(
            CustomerAddress.customer_id == customer_id, CustomerAddress.id == address_id
        )

        result = await self.session.execute(stmt)

        (address,) = result.unique().one()

        return address

    async def get_all_customer_addresses(self, customer_id: int) -> List[CustomerAddress]:

        stmt = select(CustomerAddress).where(CustomerAddress.customer_id == customer_id)

        result = await self.session.execute(stmt)

        customer_addresses = result.scalars().fetchall()

        return customer_addresses
