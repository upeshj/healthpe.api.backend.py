from fastapi import APIRouter, Depends, Security, status
from typing import List
from app.api.v1.customer.schemas.customer_schema import CreateCustomer
from ..repositories.customer_repository import CustomerRepository
from ..schemas.customer_schema import CustomerOut
from ..schemas.customer_address_schema import CustomerAddressOut
from ..usecases.usecase_create_customer import CreateCustomerUseCase
from ..usecases.usecase_add_customer_address import AddCustomerAddressUseCase
from ..usecases.usecase_get_customer_address import GetCustomerAddressUseCase
from ..usecases.usecase_get_customer_addresses import GetCustomerAllAddressUseCase
from app.core.deps import get_database
from ...auth.deps import authorize


router = APIRouter()


@router.get(
    "/{phone_number}",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    response_model=CustomerOut,
)
async def find_customer(phone_number: str, repo: CustomerRepository = Depends()):

    customer = await repo.get_customer_by_phone(phone_number)

    return customer


@router.post(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["createNewCustomer"])],
    response_model=CustomerOut,
    status_code=status.HTTP_201_CREATED,
)
async def create_new_customer(create_customer_usecase: CreateCustomerUseCase = Depends()):

    new_customer = await create_customer_usecase.handle_request()

    return new_customer


@router.post(
    "/{customer_id}/address/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    response_model=CustomerAddressOut,
    status_code=status.HTTP_201_CREATED,
)
async def create_customer_address(add_customer_address_usecase: AddCustomerAddressUseCase = Depends()):

    new_address = await add_customer_address_usecase.handle_request()

    return new_address


@router.get(
    "/{customer_id}/address/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    response_model=List[CustomerAddressOut],
)
async def create_customer_address(get_customer_addresses: GetCustomerAllAddressUseCase = Depends()):

    customer_addresses = await get_customer_addresses.handle_request()

    return customer_addresses


@router.get(
    "/{customer_id}/address/{address_id}/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    response_model=CustomerAddressOut,
)
async def create_customer_address(get_customer_address_usecase: GetCustomerAddressUseCase = Depends()):

    new_address = await get_customer_address_usecase.handle_request()

    return new_address
