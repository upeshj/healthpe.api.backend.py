import logging
from fastapi import Request, Response, Depends, HTTPException, status
from .....core.base_usecase import BaseUseCase
from ..repositories.customer_repository import CustomerRepository
from ..schemas.customer_schema import CreateCustomer

logger = logging.getLogger(__name__)


class CreateCustomerUseCase(BaseUseCase):
    def __init__(self, req: Request, res: Response, customer: CreateCustomer, repo: CustomerRepository = Depends()):
        super().__init__(req, res)

        self.customer = customer
        self.customer_repo = repo

    async def execute(self):

        existing_customer = await self.customer_repo.get_customer_by_phone(self.customer.phone_number)

        if existing_customer:
            raise HTTPException(
                status.HTTP_410_GONE, f"Customer with phone {self.customer.phone_number} alredy exists"
            )
        new_customer = await self.customer_repo.create_new_customer(self.customer)

        return new_customer
