import logging
from fastapi import Request, Response, Depends

from .....core.base_usecase import BaseUseCase
from ..repositories.customer_address_repository import CustomerAddressRepository

logger = logging.getLogger(__name__)


class GetCustomerAddressUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        customer_id: int,
        address_id: int,
        customer_address_repository: CustomerAddressRepository = Depends(),
    ):
        super().__init__(req, res)

        self.customer_id = customer_id
        self.address_id = address_id
        self.customer_address_repository = customer_address_repository

    async def execute(self):

        customer_address = await self.customer_address_repository.get_customer_address(
            self.customer_id, self.address_id
        )

        return customer_address
