import logging
from typing import Tuple
from fastapi import Request, Response, Depends, HTTPException, status

from ..repositories.customer_repository import CustomerRepository
from .....core.base_usecase import BaseUseCase
from .....core.integrations.geo.geo_service import GeoService
from app.dependencies import geo_service as gs
from app.core.integrations.geo.errors import GeoCodingError
from app.config import settings
from ..repositories.customer_address_repository import CustomerAddressRepository
from ..schemas.customer_address_schema import CreateCustomerAddress, CustomerAddressInDB

logger = logging.getLogger(__name__)


class AddCustomerAddressUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        customer_id: int,
        address: CreateCustomerAddress,
        customer_repo: CustomerRepository = Depends(),
        customer_address_repository: CustomerAddressRepository = Depends(),
        geo_coding_service: GeoService = Depends(gs),
    ):
        super().__init__(req, res)

        self.customer_id = customer_id
        self.address = address
        self.customer_repo = customer_repo
        self.customer_address_repository = customer_address_repository
        self.geo_coding_service = geo_coding_service

    async def execute(self):

        existing_customer = await self.customer_repo.get_customer_by_id(self.customer_id)

        if not existing_customer:
            raise HTTPException(status.HTTP_410_GONE, f"Customer {self.customer_id} does not exist.")

        # Do geocoding here and get long lat
        (longitude, latitude) = await self.get_long_lat()
        customer_in_db = CustomerAddressInDB(
            **self.address.dict(),
            customer_id=self.customer_id,
            long_lat=f"SRID={settings.SRID};POINT({longitude} {latitude})",
        )

        new_customer_address = await self.customer_address_repository.create_address(customer_in_db)

        return new_customer_address

    async def get_long_lat(self) -> Tuple[float, float]:

        try:
            (longitude, latitude) = await self.geo_coding_service.get_long_lat_for_address(self.address.full_address)
        except GeoCodingError as gce:
            logger.error(f"Geocoding error: {str(gce)}")
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=gce.message)

        return longitude, latitude
