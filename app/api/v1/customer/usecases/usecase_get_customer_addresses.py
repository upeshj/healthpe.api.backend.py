import logging
from typing import Tuple
from devtools import debug
from fastapi import Request, Response, Depends

from .....core.base_usecase import BaseUseCase
from app.config import settings
from ..repositories.customer_address_repository import CustomerAddressRepository

logger = logging.getLogger(__name__)


class GetCustomerAllAddressUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        customer_id: int,
        customer_address_repository: CustomerAddressRepository = Depends(),
    ):
        super().__init__(req, res)

        self.customer_id = customer_id
        self.customer_address_repository = customer_address_repository

    async def execute(self):

        customer_addresses = await self.customer_address_repository.get_all_customer_addresses(self.customer_id)

        return customer_addresses
