# from typing import Boolean
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, Date
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils import EmailType
from sqlalchemy_utils.types import PhoneNumber
from app.core.base import Base
from ...partner.models.partner_model import Partner
from app.core.base_model import HealthPeModelBase


class CustomerProfile(HealthPeModelBase):

    __tablename__ = "customer_profiles"

    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    gender = Column(String, nullable=True)
    dob = Column(Date, nullable=True)
    # username = Column(String, nullable=False)
    # hashed_password = Column(String(200), nullable=False)
    # _phone_number = Column("phone_number", Unicode(20), nullable=False)
    email = Column(EmailType, nullable=False)
    is_active = Column(Boolean, default=False)
    country_code = Column(Unicode(8), nullable=False)
    customer_id = Column(Integer, ForeignKey("customers.id"))
    # partner_code = Column(String(20), ForeignKey(Partner.code))
    # otp = Column(String(6), nullable=True)
    # last_login = Column(DateTime, nullable=False)
    # role = Column(String(10), nullable=False)

    # phone_number = sa.orm.composite(PhoneNumber, _phone_number, country_code=country_code)
