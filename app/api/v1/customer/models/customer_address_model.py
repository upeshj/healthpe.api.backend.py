# from typing import Boolean
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry
from app.core.base import Base
from app.config import settings
from app.core.base_model import HealthPeModelBase


class CustomerAddress(HealthPeModelBase):

    __tablename__ = "customer_addresses"

    id = Column(Integer, primary_key=True)
    customer_id = Column(Integer, ForeignKey("customers.id"), nullable=False)
    atype = Column(String, nullable=False, default="Home")
    line_1 = Column(String, nullable=False)
    line_2 = Column(String, nullable=True)
    city = Column(String, nullable=False)
    state = Column(String, nullable=False)
    pincode = Column(String, nullable=False)
    country = Column(String, nullable=False)
    # profile_id = Column(
    #     Integer,
    #     ForeignKey(
    #         "customer_profiles.id",
    #     ),
    # )
    is_active = Column(Boolean, nullable=False, default=True)
    long_lat = Column(Geometry("POINT", srid=settings.SRID))

    customer = relationship("Customer")
