# from typing import Boolean
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy_utils import EmailType
from sqlalchemy_utils.types import PhoneNumber, PhoneNumberType
from app.core.base import Base
from ...partner.models.partner_model import Partner
from app.core.base_model import HealthPeModelBase


class Customer(HealthPeModelBase):

    __tablename__ = "customers"

    id = Column(Integer, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=True)
    phone_number = Column(String(15), nullable=False, unique=True)
    email = Column(EmailType, nullable=True)
    is_active = Column(Boolean, default=True)
