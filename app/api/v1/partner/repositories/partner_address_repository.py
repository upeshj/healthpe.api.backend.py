from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound

from .....config import settings
from ..schemas.partner_address_schema import (
    CreatePartnerAddress,
    PartnerAddressInDB,
    PartnerAddressOut,
    PartnerAddressProximityOut,
)
from ..models.partner_model import Partner
from ..models.partner_address_model import PartnerAddress
from .....core.base_repository import BaseRepository


class PartnerAddressRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_address(self, address: PartnerAddressInDB):

        db_address = PartnerAddress(**address.dict())

        self.session.add(db_address)
        await self.session.flush()
        # await self.session.refresh(db_address)

        # stmt = insert(PartnerAddress).values(**address.dict())
        # result = await self.session.execute(stmt)

        return db_address.id

    async def find_partners_near(self, long_lat: str) -> List[PartnerAddressProximityOut]:

        debug(settings.PHARMACY_DISTANCE_THRESHOLD)
        query = (
            select(
                PartnerAddress,
                PartnerAddress.long_lat.ST_Distance(long_lat, use_spheroid=True).label("distance"),
            )
            .options(joinedload(PartnerAddress.partner))
            .where(
                PartnerAddress.long_lat.ST_Distance(long_lat, use_spheroid=True)
                <= settings.PHARMACY_DISTANCE_THRESHOLD
            )
        )

        result = await self.session.execute(query)

        # TODO: Need to generalize this to all models and custom fields
        all_addresses = []
        for address in result.fetchall():
            all_addresses.append(
                {
                    "partner_address": address["PartnerAddress"],
                    "distance": address["distance"],
                }
            )

        return all_addresses
