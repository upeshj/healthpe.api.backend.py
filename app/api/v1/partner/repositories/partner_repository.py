from typing import List
from devtools import debug
from sqlalchemy import select, insert
from sqlalchemy.exc import NoResultFound
from sqlalchemy.sql import func
from sqlalchemy.orm import joinedload

from ..schemas.partner_schema import CreatePartner, PartnerInDB
from ..models.partner_model import Partner
from ..models.partner_address_model import PartnerAddress
from .....core.base_repository import BaseRepository


class PartnerRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def get_all_partners(self) -> List[Partner]:

        result = await self.session.execute(select(Partner))

        partners = result.scalars().all()

        return partners

    async def get_partner_by_id(self, id: int) -> Partner:

        result = await self.session.execute(select(Partner).where(Partner.id == id))

        try:
            (partner,) = result.one()
            return partner
        except NoResultFound:
            return None

    async def get_partner_by_code(self, code: int) -> Partner:

        stmt = select(Partner).options(joinedload(Partner.addresses)).where(Partner.code == code)

        result = await self.session.execute(stmt)

        try:
            # Unique because there would be multiple records due to joinedload
            (partner,) = result.unique().one()
            return partner
        except NoResultFound:
            return None

    async def create_partner(self, partner: PartnerInDB) -> Partner:

        debug(**partner.dict())
        db_partner = Partner(**partner.dict())
        # stmt = insert(Partner)
        # db_partner = await self.session.execute(stmt, **db_partner)
        self.session.add(db_partner)
        await self.session.flush()
        await self.session.refresh(db_partner)
        # return db_partner
        return db_partner

    async def update_partner(self, partner_code: str, update_date: dict) -> Partner:

        db_partner = await self.get_partner_by_code(partner_code)

        return db_partner

    # async def get_partner_id_next_value(self) -> int:

    #     result = await self.session.execute(select(partner_code_seq.next_value()))

    #     (last_id,) = result.one()

    #     return last_id
