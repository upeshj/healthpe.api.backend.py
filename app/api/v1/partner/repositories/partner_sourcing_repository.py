from .....core.base_repository import BaseRepository
from ..models.partner_sourcing_model import PartnerSourcing


class PartnerSourcingRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_partner_sourcing(self, user_id: int, partner_code: int) -> None:

        new_partner_sourcing = PartnerSourcing(bd_id=user_id, partner_code=partner_code)

        self.session.add(new_partner_sourcing)
        await self.session.flush()
