from logging import debug
from typing import List
from fastapi import APIRouter, Depends, status, Security
from sqlalchemy.ext.asyncio import AsyncSession
from app.api.v1.auth.deps import authorize

from .....core.deps import get_database
from ..schemas.partner_schema import PartnerOut, CreatePartner, UpdatePartner
from ..schemas.partner_address_schema import PartnerAddressOut, PartnerAddressProximityOut
from ..repositories.partner_repository import PartnerRepository
from ..usecases.usecase_create_partner import CreatePartnerUseCase
from ..usecases.usecase_get_partner_by_code import GetPartnerbyCodeUseCase
from ..usecases.usecase_get_all_partners import GetAllPartnersUseCase
from ..usecases.usecase_add_partner_address import AddPartnerAddressUseCase
from ..usecases.usecase_get_nearby_partner_addresses import FindNearByPartnerAddresses

router = APIRouter()


@router.get("/", dependencies=[Depends(get_database)], response_model=List[PartnerOut])
async def get_all_partners(get_all_partners_usecase: GetAllPartnersUseCase = Depends()):
    """
    Returns all partners available in the system

    Args:
        get_all_partners_usecase (GetAllPartnersUseCase): Dependency

    Returns:
        List of PartnerOut objects
    """
    partners = await get_all_partners_usecase.handle_request()
    return partners


@router.get("/{partner_code}", dependencies=[Depends(get_database)], response_model=PartnerOut)
async def get_partner_by_code(get_partner_by_code_usecase: GetPartnerbyCodeUseCase = Depends()):
    """
    Returns a specific partner found by code which is the primary key

    Args:
        get_partner_by_code_usecase (GetPartnerbyCodeUseCase): Dependency

    Returns:
        ParterOut object if found, otherwise HTTP_404_NOT_FOUND
    """
    partner = await get_partner_by_code_usecase.handle_request()
    return partner


@router.put("/{partner_code}", response_model=UpdatePartner)
async def update_partner(
    partner_code: str, updated_data: UpdatePartner, session: AsyncSession = Depends(get_database)
):

    partner_repo = PartnerRepository(session)
    updated_partner = await partner_repo.update_partner(updated_data)
    return update_partner


@router.post(
    "/",
    response_model=PartnerOut,
    status_code=status.HTTP_201_CREATED,
    dependencies=[
        Depends(get_database),
        Security(authorize, scopes=["everything", "createPartner"]),
    ],
)
async def create_partner(create_partner_usecase: CreatePartnerUseCase = Depends()):
    """
    Allows creating a new partner. This would be available to internal admin
    people who can onboard new partners.

    Args:
        create_partner_usecase (CreatePartnerUseCase): Dependency

    Returns:
        PartnerOut which is newly created
    """
    new_partner = await create_partner_usecase.handle_request()

    return new_partner


@router.post(
    "/{partner_code}/address",
    dependencies=[Depends(get_database)],
    # response_model=PartnerAddressOut,
    status_code=status.HTTP_201_CREATED,
)
async def add_address(add_address_usecase: AddPartnerAddressUseCase = Depends()):

    new_address = await add_address_usecase.handle_request()

    return new_address


@router.get(
    "/nearby/addresses",
    dependencies=[Depends(get_database)],
    response_model=List[PartnerAddressProximityOut],
    status_code=status.HTTP_200_OK,
)
async def get_partner_locations(nearby_locations_usecase: FindNearByPartnerAddresses = Depends()):

    nearby_addresses = await nearby_locations_usecase.handle_request()

    debug(nearby_addresses)

    return nearby_addresses
