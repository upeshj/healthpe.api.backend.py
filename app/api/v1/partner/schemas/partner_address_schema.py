from typing import Optional
from datetime import date
from pydantic import BaseModel, constr, Field
from enum import Enum
from .....core.schema_utils import all_optional_fields
from .....core.utils import conv, single_space, joined_and_single_spaced
from .partner_schema import PartnerOut


class BasePartnerAddress(BaseModel):

    atype: str
    line_1: str
    line_2: Optional[str] = None
    landmark: Optional[str] = None
    city: str
    state: str
    pincode: str
    country: str
    gstin: str

    @property
    def full_address(self):
        return joined_and_single_spaced(
            [
                self.line_1,
                self.line_2,
                self.city,
                self.state,
                self.pincode,
                self.country,
            ],
            " ",
        )

    class Config:

        orm_mode = True


class CreatePartnerAddress(BasePartnerAddress):
    is_active: Optional[bool] = True
    is_banned: Optional[bool] = False
    drug_license_number: str
    pan_card_no: str


class PartnerAddressInDB(CreatePartnerAddress):
    """
    It represents the partner address as it's going to be stored in DB
    """

    partner_code: int
    long_lat: str


class PartnerAddressOut(CreatePartnerAddress):
    """
    This model will be used to send Partner Address information out
    along with Partner code/name etc.
    """

    partner: PartnerOut


class PartnerAddressProximityOut(BaseModel):
    """
    This model is going to be used while sending out proximity information to
    client when requesting nearest partners.
    """

    distance: int
    partner_address: PartnerAddressOut
