from typing import Optional
from pydantic import BaseModel, validator


class PartnerSearchParams(BaseModel):

    pincode: Optional[str] = None
    address: Optional[str] = None
    threshold: int = 10

    @validator("address")
    def should_have_either(cls, v, values, **kwargs):

        if v is None and values.get("pincode", None) is None:
            raise ValueError("Either address or pincode must be specified")

        return v
