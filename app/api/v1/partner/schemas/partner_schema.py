from typing import Optional
from datetime import date
from pydantic import BaseModel, constr, Field
from enum import Enum
from .....core.schema_utils import all_optional_fields
from .....core.partners_and_roles import PartnerType


class Tier(str, Enum):

    clinical = "Clinical"
    hospitals = "Hospital"
    standalone = "Standalone"
    chain = "Chain"


class BasePartner(BaseModel):

    name: constr(max_length=255)
    ptype: PartnerType

    class Config:
        json_encoders = {date: lambda v: v.isoformat()}
        orm_mode = True


class CreatePartner(BasePartner):

    is_active: bool = True
    is_banned: Optional[bool] = False
    onboarding_date: Optional[date] = Field(default_factory=date.today)
    tier: Tier


class PartnerInDB(CreatePartner):

    pass


@all_optional_fields
class UpdatePartner(CreatePartner):

    pass


class PartnerOut(PartnerInDB):

    code: int
