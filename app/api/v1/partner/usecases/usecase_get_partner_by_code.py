from fastapi import Request, Response, Depends
from fastapi.exceptions import HTTPException
from fastapi import status

from ..schemas.partner_schema import CreatePartner
from .....core.base_usecase import BaseUseCase
from ..repositories.partner_repository import PartnerRepository


class GetPartnerbyCodeUseCase(BaseUseCase):
    def __init__(
        self, req: Request, res: Response, partner_code: int, partner_repository: PartnerRepository = Depends()
    ) -> None:

        super().__init__(req, res)

        self.partner_repository = partner_repository
        self.partner_code = partner_code

    async def execute(self):

        partner = await self.partner_repository.get_partner_by_code(self.partner_code)

        if not partner:
            raise HTTPException(status.HTTP_404_NOT_FOUND, f"Partner code {self.partner_code} not found")

        return partner
