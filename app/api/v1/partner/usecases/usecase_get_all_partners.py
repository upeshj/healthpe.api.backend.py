from fastapi import Request, Response, Depends
from ..schemas.partner_schema import CreatePartner
from .....core.base_usecase import BaseUseCase
from ..repositories.partner_repository import PartnerRepository


class GetAllPartnersUseCase(BaseUseCase):
    def __init__(self, req: Request, res: Response, partner_repository: PartnerRepository = Depends()) -> None:

        super().__init__(req, res)

        self.partner_repository = partner_repository

    async def execute(self):

        partners = await self.partner_repository.get_all_partners()

        return partners
