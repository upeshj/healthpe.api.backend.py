import logging
from fastapi import Request, Response, Depends
from fastapi import HTTPException, status
from ..schemas.partner_schema import CreatePartner, PartnerInDB
from .....core.base_usecase import BaseUseCase
from ..repositories.partner_repository import PartnerRepository
from ..repositories.partner_sourcing_repository import PartnerSourcingRepository

logger = logging.getLogger(__name__)


class CreatePartnerUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        partner: CreatePartner,
        partner_repository: PartnerRepository = Depends(),
        partner_sourcing_repo: PartnerSourcingRepository = Depends(),
    ) -> None:

        super().__init__(req, res)

        self.partner_repository = partner_repository
        self.partner_sourcing_repo = partner_sourcing_repo
        self.partner = partner

    async def execute(self):

        # last_partner_id = await self.partner_repository.get_partner_id_next_value()

        # logger.warn(f"Last Partner ID: {last_partner_id}")

        # new_partner = PartnerInDB(
        #     **self.partner.dict(), code=f"{self.partner.ptype[:2].upper()}-{str(last_partner_id).zfill(9)}"
        # )
        new_partner = PartnerInDB(**self.partner.dict())

        new_partner = await self.partner_repository.create_partner(new_partner)
        await self.partner_sourcing_repo.create_partner_sourcing(self.req.state.current_user.id, new_partner.code)

        return new_partner
