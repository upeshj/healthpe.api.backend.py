import logging
from typing import Optional
from fastapi import Request, Response, Depends
from .....core.base_usecase import BaseUseCase
from .....core.integrations.geo.geo_service import GeoService
from ..schemas.partner_search_schema import PartnerSearchParams
from app.dependencies import geo_service as gs
from ..repositories.partner_address_repository import PartnerAddressRepository
from app.config import settings

logger = logging.getLogger(__name__)


def partner_search_params(address: Optional[str] = None, pincode: Optional[str] = None):

    return PartnerSearchParams(address=address, pincode=pincode)


class FindNearByPartnerAddresses(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        # address: str,
        search_params: PartnerSearchParams = Depends(partner_search_params),
        partner_address_repository: PartnerAddressRepository = Depends(),
        geo_service: GeoService = Depends(gs),
    ) -> None:

        super().__init__(req, res)

        self.geo_service = geo_service
        self.partner_address_repository = partner_address_repository
        # self.address = address
        self.search_params = search_params

    async def execute(self):

        search_term = self.search_params.address if self.search_params.address else self.search_params.pincode

        (longitude, latitude) = await self.geo_service.get_long_lat_for_address(search_term)

        logger.debug(f"Geocoding response: Longitude {longitude} latitude {latitude} for search term: {search_term}")

        long_lat = f"SRID={settings.SRID};POINT({longitude} {latitude})"

        partner_addresses = await self.partner_address_repository.find_partners_near(long_lat)

        return partner_addresses
