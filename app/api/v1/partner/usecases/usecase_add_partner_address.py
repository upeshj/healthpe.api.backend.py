import logging
from typing import Tuple
from fastapi import Request, Response, Depends, HTTPException, status
from ..schemas.partner_address_schema import CreatePartnerAddress, PartnerAddressInDB
from .....core.base_usecase import BaseUseCase
from .....core.integrations.geo.geo_service import GeoService
from app.dependencies import geo_service as gs
from app.core.integrations.geo.errors import GeoCodingError
from ..repositories.partner_address_repository import PartnerAddressRepository
from app.config import settings

logger = logging.getLogger(__name__)


class AddPartnerAddressUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        partner_code: int,
        address: CreatePartnerAddress,
        partner_address_repository: PartnerAddressRepository = Depends(),
        geo_coding_service: GeoService = Depends(gs),
    ) -> None:

        super().__init__(req, res)

        self.address = address
        self.partner_code = partner_code
        self.partner_address_repository = partner_address_repository
        self.geo_coding_service = geo_coding_service

    async def execute(self):

        (longitude, latitude) = await self.get_long_lat()

        address_in_db = PartnerAddressInDB(
            **self.address.dict(),
            partner_code=self.partner_code,
            long_lat=f"SRID={settings.SRID};POINT({longitude} {latitude})",
        )

        new_address = await self.partner_address_repository.create_address(address_in_db)

        return new_address

    async def get_long_lat(self) -> Tuple[float, float]:

        try:
            (longitude, latitude) = await self.geo_coding_service.get_long_lat_for_address(self.address.full_address)
        except GeoCodingError as gce:
            logger.error(f"Geocoding error: {str(gce)}")
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=gce.message)

        return longitude, latitude
