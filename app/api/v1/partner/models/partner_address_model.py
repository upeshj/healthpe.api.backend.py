# from typing import Boolean
import sqlalchemy as sa
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry
from app.core.base import Base
from app.config import settings
from app.core.base_model import HealthPeModelBase


class PartnerAddress(HealthPeModelBase):

    __tablename__ = "partner_addresses"

    id = Column(Integer, primary_key=True)
    atype = Column(String(15), nullable=False)
    line_1 = Column(String, nullable=False)
    line_2 = Column(String, nullable=True)
    landmark = Column(String(50), nullable=True)
    city = Column(String, nullable=False)
    state = Column(String, nullable=False)
    country = Column(String, nullable=False)
    pincode = Column(String, nullable=False)
    gstin = Column(String(15), nullable=False)

    partner_code = Column(
        Integer,
        ForeignKey(
            "partners.code",
        ),
        nullable=False,
    )
    is_active = Column(Boolean, nullable=False, default=True)
    is_banned = Column(Boolean, nullable=False, default=False)
    long_lat = Column(Geometry("POINT", srid=settings.SRID))
    # sourced_by_id = Column(Integer, ForeignKey("users.id"), nullable=True)
    drug_license_number = Column(String(30), nullable=False)  # TODO: Karan to close the uniqueness
    pan_card_no = Column(String(15), nullable=False)

    partner = relationship("Partner", back_populates="addresses")
