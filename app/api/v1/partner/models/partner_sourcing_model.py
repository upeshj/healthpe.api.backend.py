from sqlalchemy import Column, Integer, String, Boolean, Unicode, Date, Sequence, ForeignKey
from sqlalchemy.orm import relationship
from datetime import date
from app.core.base_model import HealthPeModelBase

# partner_code_seq = Sequence("partner_code_seq")


class PartnerSourcing(HealthPeModelBase):

    __tablename__ = "partner_source"

    id = Column(Integer, primary_key=True)
    partner_code = Column(Integer, ForeignKey("partners.code"))
    bd_id = Column(Integer, ForeignKey("users.id"))

    # Constraints
