from sqlalchemy import Column, Integer, String, Boolean, Unicode, Date, Sequence, ForeignKey
from sqlalchemy.orm import relationship
from datetime import date
from app.core.base import Base
from app.core.base_model import HealthPeModelBase

# partner_code_seq = Sequence("partner_code_seq")


class Partner(HealthPeModelBase):

    __tablename__ = "partners"

    code = Column(
        Integer,
        # TODO: Somehow the start is not working.
        Sequence(name="partners_code_seq", start=100001, minvalue=100001, metadata=Base.metadata),
        nullable=False,
        primary_key=True,
    )
    name = Column(String(255), nullable=False)
    is_active = Column(Boolean, nullable=False, default=True)
    is_banned = Column(Boolean, nullable=False, default=False)
    ptype = Column(String(255), nullable=False)
    onboarding_date = Column(Date, default=date.today, nullable=False)
    tier = Column(String(20), nullable=False)
    # sourced_by_id = Column(Integer, ForeignKey("users.id"), nullable=True)

    # sourced_by_associate = relationship("User", uselist=False)
    addresses = relationship("PartnerAddress", back_populates="partner")

    # Constraints
