from fastapi import APIRouter
from .partner.endpoints import partner
from .auth.endpoints import auth
from .ordering.endpoints import order
from .customer.endpoints import customer
from .payments.endpoints import payments
from .customer_invoice.endpoints import customer_invoice

# from .fulfillment.endpoints import fulfillments
# from .ordering.endpoints import fulfillments

api_router = APIRouter()
api_router.include_router(partner.router, prefix="/partner", tags=["Partner"])
api_router.include_router(auth.router, prefix="/auth", tags=["Authentication"])
api_router.include_router(order.router, prefix="/order", tags=["Ordering"])
api_router.include_router(customer.router, prefix="/customer", tags=["Customer"])
api_router.include_router(payments.router, prefix="/payment", tags=["Payments"])
api_router.include_router(customer_invoice.router, prefix="/customerinvoice", tags=["Customer Invoices"])
# api_router.include_router(fulfillments.router, prefix="/fulfillment", tags=["Fulfillment Request"])
