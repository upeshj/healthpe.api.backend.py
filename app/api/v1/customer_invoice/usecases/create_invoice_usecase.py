import logging
from datetime import date
from fastapi import Request, Response, Depends
from app.config import settings

from app.core.base_usecase import BaseUseCase
from ..schemas.customer_invoice_schema import CustomerInvoiceCreate, CustomerInvoiceInDB
from ...integrations.storage.id_proofs_storage import IDProofsStorageService
from ..repositories.customer_invoice_repository import CustomerInvoiceRepository
from ..models.customer_invoice_model import InvoiceStatus

logger = logging.getLogger(__name__)


class CreateInvoiceUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        invoice: CustomerInvoiceCreate,
        customer_invoice_repo: CustomerInvoiceRepository = Depends(),
    ):

        super().__init__(req, res)
        self.invoice = invoice
        self.customer_invoice_repo = customer_invoice_repo

    async def execute(self):

        total_tax = sum([tax.tax_amount for tax in self.invoice.taxes])
        total_amount_due = self.invoice.amount + total_tax

        today = date.today()
        customer_invoice_in_db = CustomerInvoiceInDB(
            **self.invoice.dict(),
            invoice_date=today,
            due_date=today,
            status=InvoiceStatus.Unpaid,
            total_tax=total_tax,
            total_amount_due=total_amount_due,
        )

        new_invoice = await self.customer_invoice_repo.create_invoice(customer_invoice_in_db)

        return new_invoice
