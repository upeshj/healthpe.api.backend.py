import logging
from datetime import date
from fastapi import Request, Response, Depends, HTTPException, status
from app.config import settings

from app.core.base_usecase import BaseUseCase
from ..schemas.customer_invoice_schema import CustomerInvoiceCreate, CustomerInvoiceInDB
from ...integrations.storage.id_proofs_storage import IDProofsStorageService
from ..repositories.customer_invoice_repository import CustomerInvoiceRepository
from ..models.customer_invoice_model import InvoiceStatus

logger = logging.getLogger(__name__)


class GetAllCustomerInvoicesUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        customer_id: int,
        customer_invoice_repo: CustomerInvoiceRepository = Depends(),
    ):

        super().__init__(req, res)
        self.customer_id = customer_id
        self.customer_invoice_repo = customer_invoice_repo

    async def execute(self):

        invoices = await self.customer_invoice_repo.get_all_invoices_by_customer(self.customer_id)

        return invoices
