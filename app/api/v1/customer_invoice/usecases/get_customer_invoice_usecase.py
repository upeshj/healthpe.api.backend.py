import logging
from datetime import date
from fastapi import Request, Response, Depends, HTTPException, status
from app.config import settings

from app.core.base_usecase import BaseUseCase
from ..schemas.customer_invoice_schema import CustomerInvoiceCreate, CustomerInvoiceInDB
from ...integrations.storage.id_proofs_storage import IDProofsStorageService
from ..repositories.customer_invoice_repository import CustomerInvoiceRepository
from ..models.customer_invoice_model import InvoiceStatus

logger = logging.getLogger(__name__)


class GetCustomerInvoiceUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        customer_id: int,
        invoice_id: int,
        customer_invoice_repo: CustomerInvoiceRepository = Depends(),
    ):

        super().__init__(req, res)
        self.customer_id = customer_id
        self.invoice_id = invoice_id
        self.customer_invoice_repo = customer_invoice_repo

    async def execute(self):

        invoice = await self.customer_invoice_repo.get_invoice_by_customer_and_id(self.customer_id, self.invoice_id)

        if not invoice:
            raise HTTPException(
                status.HTTP_404_NOT_FOUND,
                detail=f"Invoice Id {self.invoice_id} not found for customer {self.customer_id}",
            )

        return invoice
