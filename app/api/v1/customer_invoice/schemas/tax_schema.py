import decimal
from pydantic import BaseModel


class CustomerTax(BaseModel):

    tax_type: str
    tax_amount: decimal.Decimal

    class Config:
        orm_mode = True
