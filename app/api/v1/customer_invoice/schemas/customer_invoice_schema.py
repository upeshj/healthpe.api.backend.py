import decimal
from typing import List
from pydantic import BaseModel, Field
from datetime import date
from ..models.customer_invoice_model import InvoiceStatus
from .tax_schema import CustomerTax


class CustomerInvoiceBase(BaseModel):

    order_id: int
    customer_id: int
    amount: decimal.Decimal
    taxes: List[CustomerTax]


class CustomerInvoiceCreate(CustomerInvoiceBase):

    pass


class CustomerInvoiceInDB(CustomerInvoiceBase):

    invoice_date: date
    due_date: date
    status: InvoiceStatus
    total_tax: decimal.Decimal
    total_amount_due: decimal.Decimal


class CustomerInvoiceOut(CustomerInvoiceInDB):

    id: int = Field(alias="invoice_id")

    class Config:
        orm_mode = True
        allow_population_by_field_name = True
