import logging
import copy
from datetime import datetime
from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError

from .....config import settings
from app.core.base_repository import BaseRepository
from ..schemas.customer_invoice_schema import CustomerInvoiceInDB
from ..models.customer_invoice_model import CustomerInvoice
from ..models.customer_taxes_model import CustomerTax

logger = logging.getLogger(__name__)


class CustomerInvoiceRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_invoice(self, invoice: CustomerInvoiceInDB) -> CustomerInvoice:

        taxes_list = []

        for tax in invoice.taxes:
            taxes_list.append(CustomerTax(**tax.dict()))

        invoice_copy = copy.deepcopy(invoice.dict())
        invoice_copy.pop("taxes")
        new_invoice = CustomerInvoice(**invoice_copy, taxes=taxes_list)

        self.session.add(new_invoice)
        await self.session.flush()
        await self.session.refresh(new_invoice)

        return new_invoice

    async def get_invoice_by_customer_and_id(self, customer_id: int, invoice_id: int) -> CustomerInvoice:

        stmt = select(CustomerInvoice).where(
            CustomerInvoice.id == invoice_id, CustomerInvoice.customer_id == customer_id
        )

        result = await self.session.execute(stmt)

        try:
            (invoice,) = result.unique().one()
            return invoice
        except NoResultFound as nrf:
            logger.error(f"{str(nrf)}")
            return None

    async def get_all_invoices_by_customer(self, customer_id: int) -> List[CustomerInvoice]:

        stmt = select(CustomerInvoice).where(CustomerInvoice.customer_id == customer_id)

        result = await self.session.execute(stmt)

        try:
            invoices = []
            for (invoice,) in result.unique().fetchall():
                invoices.append(invoice)
            return invoices
        except NoResultFound as nrf:
            logger.error(f"{str(nrf)}")
            return None
