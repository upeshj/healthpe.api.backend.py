from typing import List
from fastapi import APIRouter, Depends, Security, status
from app.core.deps import get_database
from ...auth.deps import authorize
from ..schemas.customer_invoice_schema import CustomerInvoiceOut
from ..usecases.create_invoice_usecase import CreateInvoiceUseCase
from ..usecases.get_customer_invoice_usecase import GetCustomerInvoiceUseCase
from ..usecases.get_all_customer_invoices_usecase import GetAllCustomerInvoicesUseCase

router = APIRouter()


@router.post(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["createInvoice"])],
    status_code=status.HTTP_201_CREATED,
    response_model=CustomerInvoiceOut,
)
async def create_invoice(create_invoice_usecase: CreateInvoiceUseCase = Depends()):

    invoice = await create_invoice_usecase.handle_request()

    return invoice


@router.get(
    "/{customer_id}/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    status_code=status.HTTP_200_OK,
    response_model=List[CustomerInvoiceOut],
)
async def get_all_customer_invoices(get_all_customer_invoices_usecase: GetAllCustomerInvoicesUseCase = Depends()):

    invoice = await get_all_customer_invoices_usecase.handle_request()

    return invoice


@router.get(
    "/{customer_id}/{invoice_id}",
    dependencies=[Depends(get_database), Security(authorize, scopes=["seeCustomerData"])],
    status_code=status.HTTP_200_OK,
    response_model=CustomerInvoiceOut,
)
async def get_customer_invoice(get_customer_invoice_usecase: GetCustomerInvoiceUseCase = Depends()):

    invoice = await get_customer_invoice_usecase.handle_request()

    return invoice
