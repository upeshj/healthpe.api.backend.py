from sqlalchemy import Column, Integer, String, Boolean, Unicode, Date, Sequence, ForeignKey
from sqlalchemy.orm import relationship
import sqlalchemy as sa
from sqlalchemy.sql.sqltypes import DECIMAL
from app.core.base_model import HealthPeModelBase
from enum import Enum


class CustomerTax(HealthPeModelBase):

    __tablename__ = "customer_taxes"

    id = Column(Integer, primary_key=True)
    invoice_id = Column(Integer, ForeignKey("customer_invoices.id"), nullable=False)
    tax_type = Column(String(10), nullable=False)
    tax_amount = Column(DECIMAL, nullable=False)
