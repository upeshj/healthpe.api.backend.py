from sqlalchemy import Column, Integer, String, Boolean, Unicode, Date, Sequence, ForeignKey
from sqlalchemy.orm import relationship
import sqlalchemy as sa
from sqlalchemy.sql.sqltypes import DECIMAL
from app.core.base_model import HealthPeModelBase
from enum import Enum


class InvoiceStatus(str, Enum):

    Paid = "PAID"
    Unpaid = "UNPAID"


class CustomerInvoice(HealthPeModelBase):

    __tablename__ = "customer_invoices"

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, ForeignKey("orders.id"), nullable=False)
    customer_id = Column(Integer, ForeignKey("customers.id"), nullable=False)
    invoice_date = Column(Date, nullable=False)
    amount = Column(DECIMAL, nullable=False)
    total_tax = Column(DECIMAL, nullable=False)
    total_amount_due = Column(DECIMAL, nullable=False)
    due_date = Column(Date, nullable=False)
    status = Column(sa.Enum(InvoiceStatus), nullable=False, default=InvoiceStatus.Unpaid)

    taxes = relationship("CustomerTax", lazy="joined", innerjoin=True)
