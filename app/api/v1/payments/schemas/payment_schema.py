import decimal
from typing import List
from pydantic import BaseModel, Field
from ..models.payment_method_enum import PaymentMethod
from ..models.payment_status_enum import PaymentStatus


class CustomerPaymentComponents(BaseModel):

    method: PaymentMethod
    amount: decimal.Decimal
    currency: str = "INR"

    class Config:

        orm_mode = True


class CustomerPaymentBase(BaseModel):

    invoice_id: int
    amount: decimal.Decimal
    currency: str
    method: PaymentMethod
    components: List[CustomerPaymentComponents]


class InitiateCustomerPayment(CustomerPaymentBase):

    pass


class CustomerPaymentInDB(InitiateCustomerPayment):

    status: PaymentStatus


class CustomerPaymentOut(CustomerPaymentInDB):

    id: int = Field(alias="payment_id")

    class Config:

        orm_mode = True
        allow_population_by_field_name = True


class ConfirmCustomerPayment(BaseModel):

    payment_id: int
    invoice_id: int
    provider: str
    reference_code: str
