from pydantic import BaseModel


class CreatePaymentLinkSchema(BaseModel):

    order_id: int
