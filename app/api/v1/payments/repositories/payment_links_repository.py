import logging
from datetime import datetime
from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError

from .....config import settings
from app.core.base_repository import BaseRepository
from app.core.integrations.payment.payment_link_details import CorePaymentLinkDetails

from ..models.payment_link_model import PaymentLink

logger = logging.getLogger(__name__)


class PaymentLinkRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_payment_link(self, link_info: CorePaymentLinkDetails) -> PaymentLink:

        new_payment_link = PaymentLink(**link_info.dict())

        self.session.add(new_payment_link)
        await self.session.flush()
        await self.session.refresh(new_payment_link)

        return new_payment_link

    async def get_unpaid_payment_link_for_order(self, order_id: int) -> PaymentLink:

        # TODO: Use the payment _id null here when the column is created
        stmt = select(PaymentLink).where(PaymentLink.hp_order_no == order_id)

        result = await self.session.execute(stmt)

        try:
            (payment_link,) = result.one()
        except NoResultFound as nrf:
            return None
