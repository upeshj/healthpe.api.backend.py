import logging
import copy
from datetime import datetime
from operator import add
from typing import List
from devtools import debug
from sqlalchemy import select, insert, update
from sqlalchemy.orm import selectinload, joinedload
from sqlalchemy.exc import NoResultFound, IntegrityError

from .....config import settings
from app.core.base_repository import BaseRepository

from ..models.payment_model import Payment, PaymentStatus
from ..models.payment_components_model import PaymentComponent
from ..schemas.payment_schema import CustomerPaymentInDB, ConfirmCustomerPayment

logger = logging.getLogger(__name__)


class PaymentRepository(BaseRepository):
    def __init__(self):
        super().__init__()

    async def create_payment(self, payment: CustomerPaymentInDB) -> Payment:

        payment_components = []
        for component in payment.components:
            payment_components.append(PaymentComponent(**component.dict()))

        payment_copy = copy.deepcopy(payment.dict())
        payment_copy.pop("components")

        new_payment = Payment(**payment_copy, components=payment_components)
        self.session.add(new_payment)
        await self.session.flush()
        await self.session.refresh(new_payment)

        return new_payment

    async def confirm_payment(self, payment: ConfirmCustomerPayment) -> Payment:

        stmt = (
            update(Payment)
            .returning(Payment)
            .where(Payment.id == payment.payment_id, Payment.invoice_id == payment.invoice_id)
            .values(status=PaymentStatus.completed, updated_at=datetime.now())
        )

        result = await self.session.execute(stmt)

        return payment

    async def get_incomplete_payments_for_invoice_id(self, invoice_id: int) -> List[Payment]:

        stmt = select(Payment).where(Payment.invoice_id == invoice_id, Payment.status != PaymentStatus.completed)

        result = await self.session.execute(stmt)

        payments = []
        for (payment,) in result.unique().fetchall():
            payments.append(payment)

        return payments

    async def get_payment_by_id_and_invoice_id(self, payment_id: int, invoice_id: int) -> Payment:

        stmt = select(Payment).where(Payment.id == payment_id, Payment.invoice_id == invoice_id)

        result = await self.session.execute(stmt)

        try:
            (payment,) = result.unique().one()
            return payment
        except NoResultFound as nrf:
            return None
