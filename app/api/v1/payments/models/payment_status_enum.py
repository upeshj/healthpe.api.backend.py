from enum import Enum


class PaymentStatus(str, Enum):

    initiated = "Initiated"
    completed = "Completed"
