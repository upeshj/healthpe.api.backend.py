from datetime import datetime
from sqlalchemy.orm import declared_attr
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DECIMAL
from app.core.base import Base
from app.core.base_model import HealthPeModelBase
from enum import Enum


class PaymentLink(HealthPeModelBase):

    __tablename__ = "payment_links"

    id = Column(Integer, primary_key=True)
    service_provider = Column(String(20), nullable=False)
    hp_reference_no = Column(String(40), nullable=False)
    hp_order_no = Column(Integer, nullable=False)
    creation_time = Column(DateTime, nullable=False)
    amount = Column(DECIMAL, nullable=False)
    currency = Column(String(3), nullable=False)
    link_url = Column(String(255), nullable=False)
    link_expiry_time = Column(DateTime, nullable=False)
