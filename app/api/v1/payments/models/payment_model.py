from datetime import datetime
from sqlalchemy.orm import declared_attr
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DECIMAL
from app.core.base import Base
from app.core.base_model import HealthPeModelBase
from .payment_method_enum import PaymentMethod
from .payment_status_enum import PaymentStatus


class Payment(HealthPeModelBase):

    __tablename__ = "customer_payments"

    id = Column(Integer, primary_key=True)
    invoice_id = Column(Integer, ForeignKey("customer_invoices.id"), nullable=False)
    amount = Column(DECIMAL, nullable=False)
    currency = Column(String(3), nullable=False)
    method = Column(sa.Enum(PaymentMethod), nullable=False)
    status = Column(sa.Enum(PaymentStatus), nullable=False, default=PaymentStatus.initiated)

    components = relationship("PaymentComponent", lazy="joined", innerjoin=True)
