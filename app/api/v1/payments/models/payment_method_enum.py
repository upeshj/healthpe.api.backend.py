from enum import Enum


class PaymentMethod(str, Enum):

    cod = "Cash"
    online = "Online"
    # wallet = "Wallet" # Will be in V2
    # hybrid = "Hybrid" # Will be in V2
