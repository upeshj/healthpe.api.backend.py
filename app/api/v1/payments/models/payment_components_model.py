from datetime import datetime
from sqlalchemy.orm import declared_attr
from sqlalchemy import Column, Integer, String, Boolean, Unicode, DateTime
import sqlalchemy as sa
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import DECIMAL
from app.core.base import Base
from app.core.base_model import HealthPeModelBase
from .payment_method_enum import PaymentMethod


class PaymentComponent(HealthPeModelBase):

    __tablename__ = "customer_payment_components"

    id = Column(Integer, primary_key=True)
    payment_id = Column(Integer, ForeignKey("customer_payments.id"), nullable=False)
    amount = Column(DECIMAL, nullable=False)
    currency = Column(String(3), nullable=False)
    method = Column(sa.Enum(PaymentMethod), nullable=False)
