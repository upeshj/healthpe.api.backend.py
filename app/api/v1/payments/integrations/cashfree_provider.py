import logging
import httpx
from datetime import datetime
from app.config import settings
from app.core.integrations.payment.payment_link_provider import PaymentLinkProvider
from app.core.integrations.payment.payment_link_details import CorePaymentLinkDetails
from app.core.integrations.payment.errors import PaymentLinkServiceError

logger = logging.getLogger(__name__)


class CashFreePaymentProvider(PaymentLinkProvider):
    def __init__(self):

        self.url = settings.CASHFREE_PAYMENT_LINK_URL
        self.headers = {
            "x-client-id": settings.CASHFREE_APP_ID,
            "x-client-secret": settings.CASHFREE_CLIENT_SECRET,
            "x-api-version": settings.CASHFREE_PAYMENT_LINK_API_VERSION,
        }

    async def generate_payment_link(self) -> CorePaymentLinkDetails:

        logger.info("Generating payment link")

        async with httpx.AsyncClient() as client:

            payload = {
                "link_id": "234567890098765438",
                "link_amount": 100.00,
                "link_currency": "INR",
                "link_purpose": "Test payment link",
                "customer_details": {"customer_phone": "7420008555", "customer_name": "Test"},
                "link_partial_payments": False,
                "link_notify": {"send_sms": True, "send_email": False},
            }

            try:
                response = await client.post(self.url, headers=self.headers, json=payload)
            except Exception as e:
                logger.error(f"Exception occurrent while generating payment link. {str(e)}")
                raise PaymentLinkServiceError(code="P-LINK-001", message=str(e))

            status_code = response.status_code
            if status_code == 200:

                link_info = response.json()
                logger.info(f"Payment link information: {link_info}")

                creation_time = datetime.fromisoformat(link_info.get("link_created_at", datetime.now())).replace(
                    tzinfo=None
                )
                link_expiry_time = datetime.fromisoformat(link_info.get("link_expiry_time", datetime.now())).replace(
                    tzinfo=None
                )

                return CorePaymentLinkDetails(
                    service_provider="CashFree",
                    hp_reference_no="12345",
                    hp_order_no=12345,
                    creation_time=creation_time,
                    amount=link_info.get("link_amount", 100),
                    currency=link_info.get("link_currency", "INR"),
                    link_url=link_info.get("link_url", ""),
                    link_expiry_time=link_expiry_time,  # TODO: Put the right one. Discuss with Arpit
                )

            if status_code == 400:
                raise PaymentLinkServiceError(code="P-LINK-002", message=response.json().get("message", ""))
            else:
                raise PaymentLinkServiceError(code="P-LINK-003", message=response.json().get("message", ""))

    async def get_payment_link_details(self) -> CorePaymentLinkDetails:

        return None
