import logging
from datetime import datetime
from app.core.integrations.payment.payment_link_provider import PaymentLinkProvider
from app.core.integrations.payment.payment_link_details import CorePaymentLinkDetails

logger = logging.getLogger(__name__)


class RazorPayPaymentProvider(PaymentLinkProvider):
    def __init__(self):

        pass

    async def generate_payment_link(self) -> CorePaymentLinkDetails:

        raise NotImplementedError("RazorPayPaymentProvider is not implemented")

    async def get_payment_link_details(self) -> CorePaymentLinkDetails:

        raise NotImplementedError("RazorPayPaymentProvider is not implemented")
