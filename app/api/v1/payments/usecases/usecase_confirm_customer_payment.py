import logging
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from app.core.base_usecase import BaseUseCase
from app.dependencies import payment_service as ps

from ..schemas.payment_schema import ConfirmCustomerPayment
from ..models.payment_model import Payment, PaymentStatus
from ..repositories.payment_repository import PaymentRepository

logger = logging.getLogger(__name__)


class ConfirmCustomerPaymentUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        payment: ConfirmCustomerPayment,
        payment_repository: PaymentRepository = Depends(),
    ) -> None:

        super().__init__(req, res)
        self.payment = payment
        self.payment_repository = payment_repository

    async def execute(self):

        payment = await self.payment_repository.confirm_payment(self.payment)

        return payment
