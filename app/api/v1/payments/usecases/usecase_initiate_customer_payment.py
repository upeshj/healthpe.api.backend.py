import logging
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from app.core.base_usecase import BaseUseCase
from app.dependencies import payment_service as ps

from ..schemas.payment_schema import InitiateCustomerPayment
from ..schemas.payment_schema import CustomerPaymentInDB
from ..models.payment_model import Payment, PaymentStatus
from ..repositories.payment_repository import PaymentRepository

logger = logging.getLogger(__name__)


class InitiateCustomerPaymentUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        payment: InitiateCustomerPayment,
        payment_repository: PaymentRepository = Depends(),
    ) -> None:

        super().__init__(req, res)
        self.payment = payment
        self.payment_repository = payment_repository

    async def execute(self):

        existing_payments = await self.payment_repository.get_incomplete_payments_for_invoice_id(
            self.payment.invoice_id
        )

        if len(existing_payments) > 0:
            raise HTTPException(
                status.HTTP_409_CONFLICT, detail=f"Invoice {self.payment.invoice_id} already has incomplete payments."
            )

        payment_in_db = CustomerPaymentInDB(**self.payment.dict(), status=PaymentStatus.initiated)

        payment = await self.payment_repository.create_payment(payment_in_db)

        return payment
