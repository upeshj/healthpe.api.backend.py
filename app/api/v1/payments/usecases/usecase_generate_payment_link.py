import logging
from fastapi import Request, Response, Depends, Form
from fastapi import HTTPException, status
from app.core.base_usecase import BaseUseCase
from app.core.integrations.payment.payment_service import PaymentService
from app.core.integrations.payment.errors import PaymentLinkServiceError
from ..integrations.cashfree_provider import CashFreePaymentProvider
from ..repositories.payment_links_repository import PaymentLinkRepository
from app.dependencies import payment_service as ps

logger = logging.getLogger(__name__)


class GeneratePaymentLinkUseCase(BaseUseCase):
    def __init__(
        self,
        req: Request,
        res: Response,
        order_id: int = Form(...),
        payment_service: PaymentService = Depends(ps),
        payment_link_repository: PaymentLinkRepository = Depends(),
    ) -> None:

        super().__init__(req, res)
        self.order_id = order_id
        self.payment_service = payment_service
        self.payment_link_repository = payment_link_repository

    async def execute(self):

        existing_link = await self.payment_link_repository.get_unpaid_payment_link_for_order(self.order_id)

        if existing_link:
            return existing_link

        try:
            new_link = await self.payment_service.generate_payment_link()
        except PaymentLinkServiceError as plse:
            logger.error(f"Error generating payment link: {str(plse)}")
            raise HTTPException(
                status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Intenal server error. Please try again."
            )

        logger.debug(f"Link info received from payment service: {new_link}")
        await self.payment_link_repository.create_payment_link(new_link)

        return new_link
