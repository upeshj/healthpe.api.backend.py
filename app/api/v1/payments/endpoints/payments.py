from fastapi import APIRouter, Depends, Security, status
from app.core.deps import get_database
from ...auth.deps import authorize
from ..schemas.payment_schema import CustomerPaymentOut
from ..usecases.usecase_generate_payment_link import GeneratePaymentLinkUseCase
from ..usecases.usecase_initiate_customer_payment import InitiateCustomerPaymentUseCase
from ..usecases.usecase_confirm_customer_payment import ConfirmCustomerPaymentUseCase
from app.core.integrations.payment.payment_link_details import CorePaymentLinkDetails

router = APIRouter()


@router.post(
    "/link/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["canAcceptPayment"])],
    response_model=CorePaymentLinkDetails,
)
async def generate_payment_link(generate_payment_link_usecase: GeneratePaymentLinkUseCase = Depends()):

    link_info = await generate_payment_link_usecase.handle_request()

    return link_info


@router.post(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["canAcceptPayment"])],
    response_model=CustomerPaymentOut,
    status_code=status.HTTP_201_CREATED,
    summary="Initiate Payment",
)
async def initiate_payment(initiate_customer_payment_use_case: InitiateCustomerPaymentUseCase = Depends()):

    payment = await initiate_customer_payment_use_case.handle_request()

    return payment


@router.put(
    "/",
    dependencies=[Depends(get_database), Security(authorize, scopes=["canAcceptPayment"])],
    status_code=status.HTTP_200_OK,
    summary="Confirm Payment",
)
async def confirm_payment(confirm_customer_payment_use_case: ConfirmCustomerPaymentUseCase = Depends()):

    payment = await confirm_customer_payment_use_case.handle_request()

    return payment
