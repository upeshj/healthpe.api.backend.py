import logging
import os
from functools import lru_cache
from typing import Optional
from pydantic import BaseSettings

logger = logging.getLogger(__name__)


class Settings(BaseSettings):

    ENV: Optional[str] = "dev"
    TEST_MODE: Optional[bool] = True

    DB_USER: str
    DB_PASS: str
    DB_HOST: str
    DB_PORT: int
    DB_NAME: str

    AUDIT_TRAIL_DB_USER: str
    AUDIT_TRAIL_DB_PASS: str
    AUDIT_TRAIL_DB_HOST: str
    AUDIT_TRAIL_DB_PORT: int
    AUDIT_TRAIL_DB_NAME: str

    # Geo services
    GEO_SERVICE_MODULE: str
    GEO_SERVICE_CLASS: str
    SRID: Optional[int] = 4326
    GOOGLE_GEOCODING_URL: str
    GOOGLE_GEOCODING_API_KEY: str

    # SMS Services
    MSG91_OTP_URL: str
    MSG91_AUTH_KEY: str
    MSG91_OTP_TEMPLATE_ID: str

    MSG91_SMS_URL: str
    MSG91_SMS_AUTH_KEY: str

    # Payment Services
    CASHFREE_PAYMENT_LINK_URL: str
    CASHFREE_APP_ID: str
    CASHFREE_CLIENT_SECRET: str
    CASHFREE_PAYMENT_LINK_API_VERSION: str

    AWS_PROFILE: str
    ID_PROOFS_BUCKET_NAME: str
    VENDOR_INVOICE_BUCKET_NAME: str

    # Microservices
    BASE_URL: str
    CUSTOMER_SERVICE: str

    # Max distance to filter pharmacies
    PHARMACY_DISTANCE_THRESHOLD: int
    FULFILLMENT_REQUEST_TIMEOUT: int  # minutes

    # SMS Notifications
    NOTIFY_ORDER_CANCELLATION: bool = False
    NOTIFY_ORDER_CONFIRMATION: bool = False
    NOTIFY_ORDER_DELIVERY: bool = False

    class Config:

        env_file = ".env"
        case_sensitive = True


# settings = Settings(_env_file="/Users/upeshjindal/Development/HealthPe/healthpe.api.backend.py/settings/dev.env")


@lru_cache
def get_settings() -> Settings:

    logger.info(f"Calling get_settings")
    env = os.environ.get("ENV", "dev")

    if env == "dev":
        return Settings(_env_file="/Users/upeshjindal/Development/HealthPe/healthpe.api.backend.py/settings/dev.env")
    if env == "test":
        return Settings(
            _env_file="/Users/upeshjindal/Development/HealthPe/healthpe.api.backend.py/settings/test.env",
            TEST_MODE=True,
        )
    if env == "stage":
        return Settings(
            _env_file="/Users/upeshjindal/Development/HealthPe/healthpe.api.backend.py/settings/stage.env",
            TEST_MODE=False,
        )
    if env == "prod":
        return Settings(
            _env_file="/Users/upeshjindal/Development/HealthPe/healthpe.api.backend.py/settings/prod.env",
            TEST_MODE=False,
        )


settings = get_settings()
