from app.core.integrations.geo.geo_service import GeoService
from app.core.integrations.payment.payment_service import PaymentService

from .api.v1.payments.integrations.cashfree_provider import CashFreePaymentProvider
from app.core.integrations.geo.google_geo_service import GoogleGeoService


def payment_service():
    return PaymentService(CashFreePaymentProvider())


def geo_service():
    return GeoService(GoogleGeoService())
