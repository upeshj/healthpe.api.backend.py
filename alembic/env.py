import logging

# from logging import Logger
from logging.config import fileConfig
import os
from sqlalchemy import engine_from_config, create_engine
from sqlalchemy import pool
from app.config import settings
from alembic import context
from app.api import v1
from app.core.audit import models
from app.core.base import Base

logger = logging.getLogger(__name__)

EXCLUDED_TABLES = ["spatial_ref_sys"]

# from app.api.v1.auth.models.user_model import User

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = Base.metadata


def include_object(object, name, type_, reflected, compare_to):
    if type_ == "table" and name in EXCLUDED_TABLES:
        return False

    return True


from alembic.script import write_hooks
import re


@write_hooks.register("add_additional_imports")
def add_additional_imports(filename, options):

    imports_to_append = ["import sqlalchemy_utils", "import geoalchemy2"]
    create_manual_sequences = [
        # "\top.execute(sa.schema.CreateSequence(sa.Sequence('partner_code_seq')))",
    ]

    # TODO: Remove this part. And move the drop to downgrade of initial migration
    drop_manual_sequences = [
        # "\top.execute(sa.schema.DropSequence(sa.Sequence('partner_code_seq')))",
    ]
    lines = []
    # with open(filename) as file_:

    #     for line in file_:

    #         lines.append(re.sub(r"^(    )+", lambda m: "\t" * (len(m.group(1)) // 4), line))

    #         if line.startswith("import") and imports_to_append:

    #             for additional_import in imports_to_append:
    #                 lines.append(additional_import)
    #                 lines.append("\n")
    #             imports_to_append = None

    #         if line.startswith("def upgrade():") and create_manual_sequences:

    #             for create_sequence in create_manual_sequences:
    #                 lines.append(create_sequence)
    #                 lines.append("\n")
    #             create_manual_sequences = None

    #         if line.startswith("def downgrade():") and drop_manual_sequences:

    #             for drop_sequence in drop_manual_sequences:
    #                 lines.append(drop_sequence)
    #                 lines.append("\n")
    #             drop_manual_sequences = None

    # with open(filename, "w") as to_write:
    #     to_write.write("".join(lines))


# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def get_url():

    url = (
        f"postgresql://{settings.DB_USER}:{settings.DB_PASS}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}"
    )
    logger.error(url)

    return url


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    # url = config.get_main_option("sqlalchemy.url")
    url = get_url()
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    # connectable = engine_from_config(
    #     config.get_section(config.config_ini_section),
    #     prefix="sqlalchemy.",
    #     poolclass=pool.NullPool,
    # )
    connectable = create_engine(get_url())

    with connectable.connect() as connection:
        context.configure(connection=connection, target_metadata=target_metadata, include_object=include_object)

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
